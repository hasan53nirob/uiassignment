//
//  EditCustomerNameCell2.swift
//  AssignmentUI
//
//  Created by Nirob Hasan on 19/11/19.
//  Copyright © 2019 Nirob Hasan. All rights reserved.
//

import UIKit

class EditCustomerNameCell2: BaseCell {
    @IBOutlet weak var leftNameLabel: UILabel!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var textField: NHTextField!
    
    private let titleTexts = ["携帯電話番号", "車検満了日"]
    private let placeholderTexts = ["09012345678", "H31 R1 2019  1月  1日"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCellView(row: Int) {
        leftNameLabel.font = UIFont.NotoSansCJKJPRegularFont(ofSize: 15.0)
        leftNameLabel.textColor = NHTheme.shared.THEME_TEXT_COLOR

        redButton.backgroundColor = NHTheme.shared.THEME_BUTTON_RED_COLOR
        redButton.setTitleColor(NHTheme.shared.THEME_TITLE_WHITE_COLOR, for: .normal)
        redButton.round(radius: 3.0)
        
        textField.placeholder = placeholderTexts[row-1]
        leftNameLabel.text = titleTexts[row-1]
        if row == 1 {
            redButton.isHidden = false
        }
        else {
            redButton.isHidden  = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func redButtonClicked(_ sender: Any) {
        
    }
    
    // Reuser identifier
    class func reuseIdentifier() -> String {
        return "EditCustomerNameCell2"
    }
    
    // Nib name
    class func nibName() -> String {
        return "EditCustomerNameCell2"
    }
    
}
