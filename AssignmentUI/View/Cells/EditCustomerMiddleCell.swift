//
//  EditCustomerMiddleCell.swift
//  AssignmentUI
//
//  Created by Nirob Hasan on 19/11/19.
//  Copyright © 2019 Nirob Hasan. All rights reserved.
//

import UIKit

class EditCustomerMiddleCell: BaseCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: CellRoundedView!
    @IBOutlet weak var topLeftField: UITextField!
    @IBOutlet weak var topRightField: UITextField!
    @IBOutlet weak var bottomLeftField: UITextField!
    @IBOutlet weak var bottomRightField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCellView(row: Int) {
        titleLabel.font = UIFont.NotoSansCJKJPRegularFont(ofSize: 15.0)
        titleLabel.textColor = NHTheme.shared.THEME_TEXT_COLOR
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Reuser identifier
       class func reuseIdentifier() -> String {
           return "EditCustomerMiddleCell"
       }
       
       // Nib name
       class func nibName() -> String {
           return "EditCustomerMiddleCell"
       }
    
}
