//
//  EditCustomerNameCell.swift
//  AssignmentUI
//
//  Created by Nirob Hasan on 19/11/19.
//  Copyright © 2019 Nirob Hasan. All rights reserved.
//

import UIKit

class EditCustomerNameCell: BaseCell {
    @IBOutlet weak var leftNameLabel: UILabel!
    @IBOutlet weak var rightNameLabel: UILabel!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var textField: NHTextField!
    
    private let titleTexts = [""]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCellView(row: Int) {
        leftNameLabel.font = UIFont.NotoSansCJKJPRegularFont(ofSize: 15.0)
        leftNameLabel.textColor = NHTheme.shared.THEME_TEXT_COLOR
        rightNameLabel.font = UIFont.NotoSansCJKJPRegularFont(ofSize: 15.0)
        rightNameLabel.textColor = NHTheme.shared.THEME_TEXT_COLOR

        redButton.backgroundColor = NHTheme.shared.THEME_BUTTON_RED_COLOR
        redButton.setTitleColor(NHTheme.shared.THEME_TITLE_WHITE_COLOR, for: .normal)
        redButton.round(radius: 3.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func redButtonClicked(_ sender: Any) {
        
    }
    
    // Reuser identifier
    class func reuseIdentifier() -> String {
        return "EditCustomerNameCell"
    }
    
    // Nib name
    class func nibName() -> String {
        return "EditCustomerNameCell"
    }
    
}
