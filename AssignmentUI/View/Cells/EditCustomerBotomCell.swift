//
//  EditCustomerBotomCell.swift
//  AssignmentUI
//
//  Created by Nirob Hasan on 19/11/19.
//  Copyright © 2019 Nirob Hasan. All rights reserved.
//

import UIKit

class EditCustomerBotomCell: UITableViewCell {
    @IBOutlet weak var topTitle: UILabel!
    @IBOutlet weak var middleTitle: UILabel!
    @IBOutlet weak var bottomTitle: UILabel!
    @IBOutlet weak var customerEditSwitch: UISwitch!
    @IBOutlet weak var bottomLine1: UIView!
    @IBOutlet weak var bottomLine2: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCellView(row: Int) {
        topTitle.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 16.0)
        topTitle.textColor = NHTheme.shared.THEME_TEXT_COLOR
        
        middleTitle.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 16.0)
        middleTitle.textColor = NHTheme.shared.THEME_TEXT_COLOR
        
        bottomTitle.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 16.0)
        bottomTitle.textColor = NHTheme.shared.THEME_TEXT_COLOR
        
        customerEditSwitch.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        customerEditSwitch.onTintColor = NHTheme.shared.THEME_SWITCH_GRAY_COLOR // on state
        customerEditSwitch.tintColor = NHTheme.shared.THEME_SWITCH_GRAY_COLOR  // off state
        
        bottomLine1.backgroundColor = NHTheme.shared.THEME_UNDERLINE_GRAY_COLOR
        bottomLine2.backgroundColor = NHTheme.shared.THEME_UNDERLINE_GRAY_COLOR
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Reuser identifier
       class func reuseIdentifier() -> String {
           return "EditCustomerBotomCell"
       }
       
       // Nib name
       class func nibName() -> String {
           return "EditCustomerBotomCell"
       }
    @IBAction func editButtonClicked(_ sender: Any) {
        
    }
    
}
