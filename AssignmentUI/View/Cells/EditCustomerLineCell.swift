//
//  EditCustomerLineCell.swift
//  AssignmentUI
//
//  Created by Nirob Hasan on 19/11/19.
//  Copyright © 2019 Nirob Hasan. All rights reserved.
//

import UIKit

class EditCustomerLineCell: BaseCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lineButton: UIButton!
    
    private let titleTexts = ["LINE未連携", "車検証"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCellView(row: Int) {
        titleLabel.font = UIFont.NotoSansCJKJPRegularFont(ofSize: 16.0)
        titleLabel.textColor = NHTheme.shared.THEME_TEXT_COLOR.withAlphaComponent(0.7)
        titleLabel.text = titleTexts[row-4]
        
        lineButton.setTitleColor(NHTheme.shared.THEME_TITLE_GRAY_COLOR, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // Reuser identifier
    class func reuseIdentifier() -> String {
        return "EditCustomerLineCell"
    }
    
    // Nib name
    class func nibName() -> String {
        return "EditCustomerLineCell"
    }
    
}
