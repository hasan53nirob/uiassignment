//
//  BaseCell.swift
//  AssignmentUI
//
//  Created by Nirob Hasan on 19/11/19.
//  Copyright © 2019 Nirob Hasan. All rights reserved.
//

import UIKit

class BaseCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
