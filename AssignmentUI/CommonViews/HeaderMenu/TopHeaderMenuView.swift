//
//  TopMenuView.swift
//  Choku_ios
//
//  Created by chitra bonik on 30/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

protocol TopHeaderMenuViewDelegate: class {
    func calledbuttonShowFullList()
    func calledButtonShowChatList()
    func calledButtonShowCreateList()
}

class TopHeaderMenuView: UIView {

    //FullList
    @IBOutlet weak var mFullListLabel: UILabel!
    @IBOutlet weak var mFullListBottomView: UIView!
    @IBOutlet weak var mFullListButton: UIButton!
    
    //ChatList
    @IBOutlet weak var mChatListLabel: UILabel!
    @IBOutlet weak var mChatListBottomView: UIView!
    @IBOutlet weak var mChatListButton: UIButton!
    
    //CreateList
    @IBOutlet weak var mCreateListLabel: UILabel!
    @IBOutlet weak var mCreateListBottomView: UIView!
    @IBOutlet weak var mCreateListButton: UIButton!
    
    var buttonSelecting: UIButton? = nil
    weak var delegate: TopHeaderMenuViewDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setPropertiesView()
    }
    
    func setPropertiesView() {
        let scale = IS_IPAD ? DISPLAY_SCALE_IPAD : DISPLAY_SCALE
        
        // Set default
        buttonSelecting = mFullListButton
        updateStateButtons(isAllOff: false)
        
    }
    
    func updateStateButtons(isAllOff: Bool) {
        //deselect all buttton
        mFullListButton.isSelected = false
        mChatListButton.isSelected = false
        mCreateListButton.isSelected = false
        
        mFullListBottomView.isHidden = true
        mChatListBottomView.isHidden = true
        mCreateListBottomView.isHidden = true
        
        mFullListLabel.textColor = UIColor._ashLightColor()
        mChatListLabel.textColor = UIColor._ashLightColor()
        mCreateListLabel.textColor = UIColor._ashLightColor()
        
        if isAllOff == false {
            buttonSelecting?.isSelected = true
        }
        
        // For FullList
        if buttonSelecting?.tag == 0 {
            mFullListLabel.textColor = UIColor._ashDarkColor()
            mFullListBottomView.isHidden = false
        }
        
        // For ChatList
        if buttonSelecting?.tag == 1 {
            mChatListLabel.textColor = UIColor._ashDarkColor()
            mChatListBottomView.isHidden = false
        }
        
        // For CreateList
        if buttonSelecting?.tag == 2 {
            mCreateListLabel.textColor = UIColor._ashDarkColor()
            mCreateListBottomView.isHidden = false
        }
        
    }
    
    
    @IBAction func tapToFullList(_ sender: Any) {
        buttonSelecting = mFullListButton
        updateStateButtons(isAllOff: false)
        self.delegate?.calledbuttonShowFullList()
    }
    
    @IBAction func tapToChat(_ sender: Any) {
        buttonSelecting = mChatListButton
        updateStateButtons(isAllOff: false)
        self.delegate?.calledButtonShowChatList()
    }
    
    @IBAction func tapToCreate(_ sender: Any) {
        buttonSelecting = mCreateListButton
        updateStateButtons(isAllOff: false)
        self.delegate?.calledButtonShowCreateList()
    }
}
