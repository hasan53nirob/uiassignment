//
//  HeaderMenuView.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class HeaderMenuView: UIView {

    //FullList
    @IBOutlet weak var mFullListLabel: UILabel!
    @IBOutlet weak var mFullListBottomView: UIView!
    @IBOutlet weak var mFullListButton: UIButton!
    
    //ChatList
    @IBOutlet weak var mChatListLabel: UILabel!
    @IBOutlet weak var mChatListBottomView: UIView!
    @IBOutlet weak var mChatListButton: UIButton!
    
    //CreateList
    @IBOutlet weak var mCreateListLabel: UILabel!
    @IBOutlet weak var mCreateListBottomView: UIView!
    @IBOutlet weak var mCreateListButton: UIButton!
    
    var buttonSelecting: UIButton? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setPropertiesView()
    }
    
    func setPropertiesView() {
        let scale = IS_IPAD ? DISPLAY_SCALE_IPAD : DISPLAY_SCALE
        
        // Set default
        buttonSelecting = mFullListButton
        //updateStateButtons(isAllOff: false)
        
    }
    
    func updateStateButtons(isAllOff: Bool) {
        //deselect all buttton
        mFullListButton.isSelected = false
        mChatListButton.isSelected = false
        mCreateListButton.isSelected = false
        
        mFullListBottomView.backgroundColor = UIColor.white
        mChatListBottomView.backgroundColor = UIColor.white
        mCreateListBottomView.backgroundColor = UIColor.white
        
        
        if isAllOff == false {
            buttonSelecting?.isSelected = true
        }
        
        // For FullList
        if buttonSelecting?.tag == 0 {
            mFullListBottomView.backgroundColor = UIColor.red
        }
        
        // For ChatList
        if buttonSelecting?.tag == 1 {
            mChatListBottomView.backgroundColor = UIColor.red
        }
        
        // For CreateList
        if buttonSelecting?.tag == 2 {
            mCreateListBottomView.backgroundColor = UIColor.red
        }
        
    }

}
