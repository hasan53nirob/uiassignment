//
//  CustomTabbarController.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class CustomTabbarController: UITabBarController {

    var viewTabbar = CustomTabbarView.shared
    var isHiddenTabbar : Bool = false
    var home: TopHomeViewController? = nil
    //var customerList:CustomerListNavigationController? = nil
    //var scan:ScanNavigationController? = nil
    //var notice:NoticeNavigationController? = nil
    //var delivery:DeliveryNavigationController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createSubviews()
        setPropertiesOfViewControllers()
        Utils.customTabbarVC = self
    }


    //MARK: - Getter
    func setHideTitleTabar(isHidden: Bool) {
        isHiddenTabbar = isHidden
        if isHidden == true {
            UIView.animate(withDuration: 0.4, animations: {
                self.viewTabbar.frame = CGRect(x: 0, y: SIZE_HEIGHT, width: SIZE_WIDTH, height: (69 * DISPLAY_SCALE + Utils.getBottomPadding()))
                self.view.layoutIfNeeded()
            })
        }
        else {
            self.viewTabbar.frame = CGRect(x: 0, y: SIZE_HEIGHT - (69 * DISPLAY_SCALE + Utils.getBottomPadding()), width: SIZE_WIDTH, height: (69 * DISPLAY_SCALE + Utils.getBottomPadding()))
            self.view.layoutIfNeeded()
        }
    }
    
    // Private function
    private func createSubviews() {
        self.view.addSubview(self.viewTabbar)
        self.viewTabbar.delegate = self
        self.viewTabbar.frame = CGRect(x: 0, y: SIZE_HEIGHT - (69 * DISPLAY_SCALE + Utils.getBottomPadding()), width: SIZE_WIDTH, height: (69 * DISPLAY_SCALE + Utils.getBottomPadding()))
    }
    
    private func setPropertiesOfViewControllers() {
        
        let storyBoardHome: UIStoryboard = UIStoryboard(name: "TopHome", bundle: nil)
        self.home = storyBoardHome.instantiateViewController(withIdentifier: "TopHomeViewController") as? TopHomeViewController
        
        /*let storyBoardList: UIStoryboard = UIStoryboard(name: "CustomerList", bundle: nil)
        self.customerList = storyBoardList.instantiateViewController(withIdentifier: "CustomerListNavigationController") as? CustomerListNavigationController
        
        let storyboardScan = UIStoryboard(name: "Scan", bundle: nil)
        self.scan = storyboardScan.instantiateViewController(withIdentifier: "ScanNavigationController") as? ScanNavigationController
        
        let storyboardNotice = UIStoryboard(name: "Notice", bundle: nil)
        self.notice = storyboardNotice.instantiateViewController(withIdentifier: "NoticeNavigationController") as? NoticeNavigationController
        
        let storyboardDelivery = UIStoryboard(name: "Delivery", bundle: nil)
        self.delivery = storyboardDelivery.instantiateViewController(withIdentifier: "DeliveryNavigationController") as? DeliveryNavigationController*/
        
        self.viewControllers = [self.home!]
    }

}

extension CustomTabbarController: CustomTabbarDelegate {
    func calledbuttonHome() {
        self.selectedIndex = 0
    }
    
    func calledbuttonCustomerList() {
        self.selectedIndex = 1
    }
    
    func calledButtonScan() {
        self.selectedIndex = 2
    }
    
    func calledButtonNotice() {
        self.selectedIndex = 3
    }
    
    func calledButtonDelivery() {
        self.selectedIndex = 4
    }
    
}
