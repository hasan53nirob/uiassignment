//
//  CustomTabbarView.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

protocol CustomTabbarDelegate: class {
    func calledbuttonHome()
    func calledbuttonCustomerList()
    func calledButtonScan()
    func calledButtonNotice()
    func calledButtonDelivery()
}

class CustomTabbarView: UIView {

    var buttonSelecting: UIButton? = nil
    @IBOutlet weak var imageHome: UIImageView!
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var buttonHome: UIButton!
    
    @IBOutlet weak var imageCustomerList: UIImageView!
    @IBOutlet weak var listLabel: UILabel!
    @IBOutlet weak var buttonCustomerList: UIButton!
    
    @IBOutlet weak var imageScan: UIImageView!
    @IBOutlet weak var scanLabel: UILabel!
    @IBOutlet weak var buttonScan: UIButton!
    
    @IBOutlet weak var imageNotice: UIImageView!
    @IBOutlet weak var noticeLabel: UILabel!
    @IBOutlet weak var buttonNotice: UIButton!
    
    @IBOutlet weak var imageDelivery: UIImageView!
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var buttonDelivery: UIButton!
    weak var delegate: CustomTabbarDelegate? = nil
    
    static let shared: CustomTabbarView = {
        let scale = IS_IPAD ? DISPLAY_SCALE_IPAD : DISPLAY_SCALE
        let view:CustomTabbarView = Bundle.main.loadNibNamed("CustomTabbarView", owner: self, options: nil)?[0] as! CustomTabbarView
        var height: CGFloat = 69.0
        if #available(iOS 11.0, *) {
            if let window = UIApplication.shared.keyWindow {
                height = height + window.safeAreaInsets.bottom
            }
        }
        view.frame = CGRect(x: 0, y: SIZE_HEIGHT - height, width: SIZE_WIDTH, height: height)
        view.layoutIfNeeded()
        
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setPropertiesView()
    }
    
    func setPropertiesView() {
        let scale = IS_IPAD ? DISPLAY_SCALE_IPAD : DISPLAY_SCALE
        
        // Set default
        buttonSelecting = buttonCustomerList
        updateStateButtons(isAllOff: false)
        
    }
    
    func updateStateButtons(isAllOff: Bool) {
        //deselect all buttton
        buttonHome.isSelected = false
        buttonCustomerList.isSelected = false
        buttonScan.isSelected = false
        buttonNotice.isSelected = false
        buttonDelivery.isSelected = false
        
        //set Label
        homeLabel.text = UIText.HomeLabel.localized
        listLabel.text = UIText.CustomerListLabel.localized
        scanLabel.text = UIText.ScanLabel.localized
        noticeLabel.text = UIText.NoticeLabel.localized
        deliveryLabel.text = UIText.DeliveryLabel.localized
        
        homeLabel.textColor = UIColor._ashLightTextColor()
        listLabel.textColor = UIColor._ashLightTextColor()
        scanLabel.textColor = UIColor._ashLightTextColor()
        noticeLabel.textColor = UIColor._ashLightTextColor()
        deliveryLabel.textColor = UIColor._ashLightTextColor()
        
        //set image
        imageHome.image = UIImage(named: "home24")
        imageCustomerList.image = UIImage(named: "customer_list")
        imageScan.image = UIImage(named: "scan_footer")
        imageNotice.image = UIImage(named: "footer_notification")
        imageDelivery.image = UIImage(named: "footer_delivery")
        
        if isAllOff == false {
            buttonSelecting?.isSelected = true
        }
        
        // For button01
        if buttonSelecting?.tag == 0 {
            imageHome.image = buttonSelecting!.isSelected ? UIImage(named: "home24") : UIImage(named: "home24")
            homeLabel.textColor = UIColor._ashDarkTextColor()
        }
        
        // For button02
        if buttonSelecting?.tag == 1 {
            imageCustomerList.image = buttonSelecting!.isSelected ? UIImage(named: "customer_list_select") : UIImage(named: "customer_list")
            listLabel.textColor = UIColor._ashDarkTextColor()
        }
        
        // For button03
        if buttonSelecting?.tag == 2 {
            imageScan.image = buttonSelecting!.isSelected ? UIImage(named: "scan_footer_select") : UIImage(named: "scan_footer")
            scanLabel.textColor = UIColor._ashDarkTextColor()
        }
        
        // For button04
        if buttonSelecting?.tag == 3 {
            imageNotice.image = buttonSelecting!.isSelected ? UIImage(named: "footer_notification_select") : UIImage(named: "footer_notification")
            noticeLabel.textColor = UIColor._ashDarkTextColor()
        }
        
        // For button05
        if buttonSelecting?.tag == 4 {
            imageDelivery.image = buttonSelecting!.isSelected ? UIImage(named: "footer_delivery_select") : UIImage(named: "footer_delivery")
            deliveryLabel.textColor = UIColor._ashDarkTextColor()
        }
    }
    
    @IBAction func tapToHome(_ sender: Any?) {
        buttonSelecting = buttonHome
        updateStateButtons(isAllOff: false)
        self.delegate?.calledbuttonHome()
    }
    
    @IBAction func tapToCustomerList(_ sender: Any?) {
        buttonSelecting = buttonCustomerList
        updateStateButtons(isAllOff: false)
        self.delegate?.calledbuttonCustomerList()
    }
    
    
    @IBAction func tapToScan(_ sender: Any?) {
        buttonSelecting = buttonScan
        updateStateButtons(isAllOff: false)
        self.delegate?.calledButtonScan()
    }
    
    @IBAction func tapToNotice(_ sender: Any?) {
        buttonSelecting = buttonNotice
        updateStateButtons(isAllOff: false)
        self.delegate?.calledButtonNotice()
    }
    
    @IBAction func tapToDelivery(_ sender: Any?) {
        buttonSelecting = buttonDelivery
        updateStateButtons(isAllOff: false)
        self.delegate?.calledButtonDelivery()
    }
    
}
