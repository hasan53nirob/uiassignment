//
//  CustomNavigationBar.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

@objc protocol CustomNavigationBarDelegate: class {
    @objc optional func didClickBackButton()
    @objc optional func didClickLeftButton()
    @objc optional func didClickRightButton()
}

class CustomNavigationBar: UIView {
    
    
    private var buttonBack: UIButton = UIButton()
    private var buttonLeft: UIButton = UIButton()
    private var buttonRight: UIButton = UIButton()
    
    var titleNavBar: UILabel = UILabel()
    private var shadowImage:UIImageView = UIImageView()
    private var title: String?
    private var leftButtonTitle: String?
    private var rightButtonTitle: String?
    private var scale: CGFloat = DISPLAY_SCALE
    private let shadowHeight: CGFloat = 0.5
    let overlayView = UIView()
    var isMenuExpanded: Bool = false
    
    var delegate: CustomNavigationBarDelegate?
    
    var menuView: TopMenuView = {
        let view: TopMenuView = (Bundle.main.loadNibNamed("TopMenuView", owner: self, options: nil))?[0] as! TopMenuView
        view.frame = CGRect(x:0 , y: 0, width: 254.0 * DISPLAY_SCALE, height: SIZE_HEIGHT)
        view.isHidden = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        scale = IS_IPAD ? DISPLAY_SCALE_IPAD : DISPLAY_SCALE
        setTitleLabel()
        addShadow()
        initializeLeftRightButton()
        setSubviews(isShowShadow: false)
        layoutIfNeeded()
    }
    
    // Set title label
    private func setTitleLabel() {
        titleNavBar.text = title
        titleNavBar.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 16.0 * scale)
        titleNavBar.textColor = .white
        titleNavBar.sizeToFit()
        let originY = (frame.size.height - titleNavBar.frame.size.height) / 2
        titleNavBar.frame =  CGRect(x: 50.scale(), y: originY - 5.scale(), width: 275.scale(), height: 24.scale())
        titleNavBar.textAlignment = .center
    }
    
    // Set back button
    func setBackButton(isShowShadow: Bool) {
        let buttonSize = 70.0 * DISPLAY_SCALE
        let buttonX: CGFloat = 0.0
        var buttonY: CGFloat = 0.0
        let buttonHeight = self.frame.height
        if isShowShadow == true {
            buttonY = buttonY - shadowHeight
        }
        buttonBack.frame = CGRect(x: buttonX, y: buttonY, width: buttonSize, height: buttonHeight)
        
        buttonBack.setImage(UIImage(named: "button_back"), for: .normal)
        buttonBack.setImage(UIImage(named: "button_back"), for: .selected)
        buttonBack.setImage(UIImage(named: "button_back"), for: .highlighted)
        
        buttonBack.addTarget(self, action: #selector(self.clickButtonBack), for: .touchUpInside)
        //buttonBack.backgroundColor = .red
        //let yPosition = self.titleNavBar.center.y - buttonY
        buttonBack.imageEdgeInsets = UIEdgeInsets(top: -(9.scale()), left: -(17.scale()), bottom: 0, right: 0)
        
    }
    
    // Set left button
    func setLeftButton() {
        let labelLeftButtonTitle = UILabel()
        if self.leftButtonTitle != nil {
            labelLeftButtonTitle.text = self.leftButtonTitle
            labelLeftButtonTitle.textColor = UIColor.white
            labelLeftButtonTitle.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 16.0 * DISPLAY_SCALE)
            labelLeftButtonTitle.sizeToFit()
        } else {
            buttonLeft.setImage(UIImage(named: "menu_icon"), for: .normal)
            buttonLeft.setImage(UIImage(named: "menu_icon"), for: .selected)
            buttonLeft.setImage(UIImage(named: "menu_icon"), for: .highlighted)
        }
        
        let buttonWidth = 70.0 * DISPLAY_SCALE
        let buttonHeight = self.frame.height
        let buttonX: CGFloat = 0 * DISPLAY_SCALE
        let buttonCenterY: CGFloat = self.titleNavBar.center.y 
        
        buttonLeft.imageView?.contentMode = .scaleAspectFit
        
        self.buttonLeft.frame.size = CGSize(width: buttonWidth,
                                            height: buttonHeight)
        self.buttonLeft.frame.origin.x = buttonX
        self.buttonLeft.center.y = buttonCenterY
//        self.buttonLeft.backgroundColor = .red
        
        labelLeftButtonTitle.frame.origin.x = buttonX
        labelLeftButtonTitle.center.y = self.titleNavBar.center.y
//        self.addSubview(labelLeftButtonTitle)
//        self.sendSubviewToBack(labelLeftButtonTitle)
        
        self.buttonLeft.addTarget(self, action: #selector(self.clickButtonLeft(_:)), for: .touchUpInside)
        setMenu()
        configureGestures()
    }
    
    // Set right button
    func setRighttButton() {
        let labelRightButtonTitle = UILabel()
        labelRightButtonTitle.text = self.rightButtonTitle
        labelRightButtonTitle.textColor = UIColor.white
        labelRightButtonTitle.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 16.0 * DISPLAY_SCALE)
        labelRightButtonTitle.sizeToFit()
        labelRightButtonTitle.center.y = self.titleNavBar.center.y + 5
        
        self.buttonRight.setTitleColor(UIColor.white, for: .normal)
        self.buttonRight.titleLabel?.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 16.0 * DISPLAY_SCALE)
        
        let buttonWidth = UIScreen.main.bounds.size.width * (100.0 / 375.0)
        let buttonHeight = self.frame.height - 20.0
        let buttonX: CGFloat = SIZE_WIDTH - buttonWidth
        let buttonCenterY: CGFloat = self.titleNavBar.center.y
        
        self.buttonRight.frame.size = CGSize(width: buttonWidth,
                                             height: buttonHeight)
        self.buttonRight.frame.origin.x = buttonX
        self.buttonRight.center.y = buttonCenterY
        
        labelRightButtonTitle.frame.origin.x = SIZE_WIDTH - labelRightButtonTitle.frame.size.width - (16 * DISPLAY_SCALE)
        labelRightButtonTitle.center.y = self.titleNavBar.center.y
        self.addSubview(labelRightButtonTitle)
        self.sendSubviewToBack(labelRightButtonTitle)
        
        self.buttonRight.addTarget(self, action: #selector(self.clickButtonRight(_:)), for: .touchUpInside)
    }
    
    func setSubviews(isShowShadow: Bool) {
        backgroundColor = .clear
        addSubview(titleNavBar)
        addSubview(buttonBack)
        if isShowShadow == true {
            addSubview(shadowImage)
        }
    }
    
    private func addShadow() {
        shadowImage = UIImageView(frame: CGRect(x: 0, y: frame.size.height*scale - shadowHeight, width: SIZE_WIDTH, height: shadowHeight))
        shadowImage.backgroundColor = UIColor.clear
        shadowImage.image = UIImage.init(named: "shadowLine")
    }
    
    // Set Title of the label
    func setTitle(_title: String?, hideBackButton: Bool = false, hideLeftButton: Bool = false, hideRightButton: Bool = false) {
        if hideBackButton {
            buttonBack.isHidden = true
        }
        if hideLeftButton {
            buttonLeft.isHidden = true
        }
        if hideRightButton {
            buttonRight.isHidden = true
        }
        title = _title
        setTitleLabel()
    }
    
    func setFont(font: UIFont?) {
        if let _font = font {
            titleNavBar.font = _font
        }
    }
    
    func changeIconCloseButton() {
        buttonBack.setImage(UIImage(named: "button_back"), for: .normal)
        buttonBack.setImage(UIImage(named: "button_back"), for: .selected)
        buttonBack.setImage(UIImage(named: "button_back"), for: .highlighted)
    }
    
    func changeIconLeftBackButton() {
        buttonBack.setImage(UIImage(named: "button_back"), for: .normal)
        buttonBack.setImage(UIImage(named: "button_back"), for: .selected)
        buttonBack.setImage(UIImage(named: "button_back"), for: .highlighted)
    }
    
    // Activate left-right button
    func activateLeftRightButton(leftText: String?, rightText: String?) {
        // Disable back button
        self.buttonBack.isHidden = true
        self.buttonBack.isEnabled = false
        
        // Show and enable left button
        if let _leftText = leftText, leftText!.count > 0 {
            self.leftButtonTitle = _leftText
            self.setLeftButton()
            self.buttonLeft.isHidden = false
            self.buttonLeft.isEnabled = true
        }
        
        // Show and enable right button
        if let _rightText = rightText, rightText!.count > 0  {
            self.rightButtonTitle = _rightText
            self.setRighttButton()
            self.buttonRight.isHidden = false
            self.buttonRight.isEnabled = true
        }
    }
    
    // Activate left-right button
    func activateRightButtonWithImage(rightImage: UIImage) {
        // Disable back button
        self.buttonBack.isHidden = true
        self.buttonBack.isEnabled = false
        
        // Show and enable right button
        self.setRighttButton()
        let originX = SIZE_WIDTH - 20.scale() - 15.scale()
        let buttonRightImageView = UIImageView(frame: CGRect(x: originX, y: 0.0, width: 20.scale(), height: 20.scale()))
        buttonRightImageView.center.y = self.titleNavBar.center.y
        
        buttonRightImageView.image = rightImage
        addSubview(buttonRightImageView)
        sendSubviewToBack(buttonRightImageView)
        
        self.buttonRight.isHidden = false
        self.buttonRight.isEnabled = true
    }
    
    // Iniitialize left right button
    public func initializeLeftRightButton() {
        // Disable left button
        self.buttonLeft.isHidden = false
        self.buttonLeft.isEnabled = true
        self.addSubview(self.buttonLeft)
        
        // Disable right button
        self.buttonRight.isHidden = false
        self.buttonRight.isEnabled = true
        self.addSubview(self.buttonRight)
    }
    
    //MARK: Menu
    func setMenu() {
        if let _window = UIApplication.shared.keyWindow {
            overlayView.frame = _window.frame
            _window.addSubview(overlayView)
            _window.addSubview(menuView)

        }
        
        self.menuView.transform = CGAffineTransform(translationX: -(254.0 * DISPLAY_SCALE), y: 0)
        overlayView.backgroundColor = .clear
        overlayView.alpha = 0
        menuView.delegate = self
    }
    
    private func configureGestures() {
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(didSwipeLeft))
        swipeLeftGesture.direction = .left
        overlayView.addGestureRecognizer(swipeLeftGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOverlay))
        overlayView.addGestureRecognizer(tapGesture)
    }
    
    //MARK: Objective-C func
    
    @objc fileprivate func didSwipeLeft() {
        toggleMenu()
    }
    
    @objc fileprivate func didTapOverlay() {
        toggleMenu()
    }
    
    private func toggleMenu() {
        isMenuExpanded = !isMenuExpanded
        let xPosition: CGFloat = (isMenuExpanded) ? 0.0 : -(self.menuView.frame.width)
        UIView.animate(withDuration: 0.3, animations: {
            self.menuView.transform = CGAffineTransform(translationX: xPosition, y: 0)
            self.overlayView.alpha = (self.isMenuExpanded) ? 0.5 : 0.0
        }) { (success) in
        }
    }
    
    
    @objc func clickButtonLeft(_ sender: UIButton) {
        toggleMenu()
        if let _delegate = delegate {
            _delegate.didClickLeftButton!()
        }
    }
    
    @objc func clickButtonRight(_ sender: UIButton) {
        if let _delegate = delegate {
            _delegate.didClickRightButton!()
        }
    }
    
    @objc func clickButtonBack(_ sender: UIButton) {
        if let _delegate = delegate {
            _delegate.didClickBackButton!()
        }
    }

}

extension CustomNavigationBar: TopMenuViewDelegate {
    func closeMenuView() {
        toggleMenu()
    }
    
}
