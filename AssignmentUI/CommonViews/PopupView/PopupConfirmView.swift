//
//  PopupConfirmView.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

public protocol PopupConfirmViewDelegate: class {
    func closeAllView()
}

class PopupConfirmView: UIView {

    @IBOutlet weak var mtitleLabel: UILabel!
    @IBOutlet weak var mCancelButton: UIButton!
    @IBOutlet weak var mConfirmButton: UIButton!
    public var screenName: String?
    var delegate: PopupConfirmViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFontForViews()
    }
    
    private func setFontForViews() {
        mtitleLabel.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 18.scale())
    }
    
    func show(title: String) {
        self.mtitleLabel.text = title
        self.isHidden = false
        if let _window = UIApplication.shared.keyWindow {
            _window.addSubview(self)
        }
    }
    
    func hide() {
        self.isHidden = true
        self.removeFromSuperview()
    }
    
    //Button Action
    @IBAction func tapForCancel(_ sender: Any) {
        hide()
    }
    
    @IBAction func tapForConfirm(_ sender: Any) {
        hide()
        if screenName == "logout" {
            Utils.removeAllSaveData {
                if let _delegate = self.delegate {
                    _delegate.closeAllView()
                }
            }
        }

    }
    
}
