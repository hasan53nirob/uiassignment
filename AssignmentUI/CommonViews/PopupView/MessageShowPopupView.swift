//
//  MessageShowPopupView.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class MessageShowPopupView: UIView {

    @IBOutlet weak var mMessageBackgroundView: UIView!
    @IBOutlet weak var mMessageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFontForViews()
    }
    
    private func setFontForViews() {
    }
    
    private func setPropertiesOfViews(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapSelect))
        addGestureRecognizer(tapGesture)
    }
    
    func show(message: String) {
        self.mMessageLabel.text = message
        if let _window = UIApplication.shared.keyWindow {
            _window.addSubview(self)
            self.isHidden = false
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.didTapSelect()
        }
    }
    
    @objc private func didTapSelect() {
        self.isHidden = true
        self.removeFromSuperview()
    }
    
}
