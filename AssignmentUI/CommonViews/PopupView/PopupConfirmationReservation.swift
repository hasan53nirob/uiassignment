//
//  PopupConfirmationReservation.swift
//  Choku_ios
//
//  Created by chitra bonik on 24/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class PopupConfirmationReservation: UIView {

    @IBOutlet weak var mtitleLabel: UILabel!
    @IBOutlet weak var mCancelButton: UIButton!
    @IBOutlet weak var mConfirmButton: UIButton!
    public var screenName: String?
    public var mainViewHeight: CGFloat?
    @IBOutlet weak var mViewHeightConstraint: NSLayoutConstraint!
    
    var popupConfirm: PopupConfirmation = {
        let view: PopupConfirmation = (Bundle.main.loadNibNamed("PopupConfirmation", owner: self, options: nil))?[0] as! PopupConfirmation
        view.frame = CGRect(x:0 , y: 0, width: SIZE_WIDTH, height: SIZE_HEIGHT)
        view.isHidden = false
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setPropertiesForViews()
    }
    
    private func setPropertiesForViews() {
        mtitleLabel.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 18.scale())
    }
    
    func show(title: String) {
        self.mtitleLabel.text = title
        mViewHeightConstraint.constant = mainViewHeight ?? 254.0 * DISPLAY_SCALE
        self.isHidden = false
        if let _window = UIApplication.shared.keyWindow {
            _window.addSubview(self)
        }
    }
    
    func hide() {
        self.isHidden = true
        self.removeFromSuperview()
    }
    
    //Button Action
    @IBAction func tapForCancel(_ sender: Any) {
        hide()
    }
    
    @IBAction func tapForConfirm(_ sender: Any) {
        hide()
        popupConfirm.show(title: UIText.popupDeliveryConfirmationDoneTitle.localized, buttonTitle: UIText.ReturnText.localized)
    }
}
