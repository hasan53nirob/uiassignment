//
//  PopupInformationView.swift
//  Choku_ios
//
//  Created by chitra bonik on 24/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class PopupInformationView: UIView {
    
    @IBOutlet weak var mtitleLabel: UILabel!
    @IBOutlet weak var mCancelButton: UIButton!
    @IBOutlet weak var mConfirmButton: UIButton!
    public var screenName: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFontForViews()
    }
    
    private func setFontForViews() {
        mtitleLabel.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 18.scale())
    }
    
    func show(title: String) {
        self.mtitleLabel.text = title
        self.isHidden = false
        if let _window = UIApplication.shared.keyWindow {
            _window.addSubview(self)
        }
    }
    
    func hide() {
        self.isHidden = true
        self.removeFromSuperview()
    }
    
    //Button Action
    @IBAction func tapForCancel(_ sender: Any) {
        hide()
    }
    
    @IBAction func tapForConfirm(_ sender: Any) {
        hide()
        
    }
    
}
