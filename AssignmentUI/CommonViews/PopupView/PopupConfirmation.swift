//
//  PopupConfirmation.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class PopupConfirmation: UIView {

    @IBOutlet weak var mtitleLabel: UILabel!
    @IBOutlet weak var mConfirmButton: UIPrimaryButtonSmall!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFontForViews()
    }
    
    private func setFontForViews() {
        mtitleLabel.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 18.scale())
    }
    
    func show(title: String, buttonTitle: String) {
        self.mtitleLabel.text = title
        mConfirmButton.setTitle(buttonTitle, for: .normal)
        self.isHidden = false
        if let _window = UIApplication.shared.keyWindow {
            _window.addSubview(self)
        }
    }
    
    func hide() {
        self.isHidden = true
        self.removeFromSuperview()
    }
    
    @IBAction func tapForConfirmation(_ sender: Any) {
        hide()
    }

}
