//
//  BaseViewController.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    private var isSetForegroundView: Bool = false
    private var _foregroundKeyboardView: UIView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHideNavBar(isHidden: true)
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Getter
    var foregroundKeyboardView:UIView {
        if _foregroundKeyboardView == nil {
            _foregroundKeyboardView = UIView(frame: CGRect(x: 0, y: 0, width: SIZE_WIDTH, height: SIZE_HEIGHT))
            let tapView: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(BaseViewController.hideForegroundKeyboardView))
            _foregroundKeyboardView!.addGestureRecognizer(tapView)
            _foregroundKeyboardView!.isHidden = true
        }
        return _foregroundKeyboardView!
    }
    
    //MARK:- Public method
    func addForegroundKeyboard(parentView:UIView) {
        if isSetForegroundView == false {
            parentView.addSubview(self.foregroundKeyboardView)
            isSetForegroundView = true
        }
    }
    
    @objc func hideForegroundKeyboardView() {
        self.setEditing(false, animated: false)
        
    }
    func showForegroundKeyboardView() {
        self.foregroundKeyboardView.isHidden = false
    }
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func setHideNavBar(isHidden: Bool){
        self.navigationController?.isNavigationBarHidden = isHidden
    }
    
    // MARK: - MBProgressHUD
    @objc func hideHUDProgress() {
        ProgressHUD.shared.hide()
    }
    
    func showHUDProgress() {
        ProgressHUD.shared.show()
    }

    
}
