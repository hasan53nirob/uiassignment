//
//  CustomerInfoCell.swift
//  AssignmentUI
//
//  Created by mac pro-t1 on 11/26/19.
//  Copyright © 2019 Nirob Hasan. All rights reserved.
//

import UIKit

class CustomerInfoCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellView(row: Int) {
        
    }
    
    // Reuser identifier
    class func reuseIdentifier() -> String {
        return "CustomerInfoCell"
    }
    
    // Nib name
    class func nibName() -> String {
        return "CustomerInfoCell"
    }
    
}
