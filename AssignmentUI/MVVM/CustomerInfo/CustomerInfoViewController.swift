//
//  CustomerInfoViewController.swift
//  AssignmentUI
//
//  Created by mac pro-t1 on 11/26/19.
//  Copyright © 2019 Nirob Hasan. All rights reserved.
//

import UIKit

class CustomerInfoViewController: BaseViewController {
    @IBOutlet weak var mNavigationBar: CustomNavigationBar!
    @IBOutlet weak var customerInfoTableView: UITableView!
    
    let titleTexts = ["氏名", "氏名（カタカナ）"]
    let descriptionTexts = ["田中 太郎", "タナカ タロウ"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = NHTheme.shared.THEME_BG_COLOR
        self.setUITexts()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func setupTableView() {
        let nib = UINib(nibName: CustomerInfoCell.nibName(), bundle: nil)
        customerInfoTableView.register(nib, forCellReuseIdentifier: CustomerInfoCell.reuseIdentifier())
    }
    
    class func getIdentifier() -> String {
        return "CustomerInfoViewController"
    }
    
    // Setup UITexts
    private func setUITexts() {
        self.mNavigationBar.setTitle(_title: "顧客情報", hideBackButton: false, hideLeftButton: true, hideRightButton: true)
        self.mNavigationBar.activateLeftRightButton(leftText: nil, rightText: "編集")
        self.mNavigationBar.setBackButton(isShowShadow: true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

}

extension CustomerInfoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
}

extension CustomerInfoViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleTexts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CustomerInfoCell.reuseIdentifier()) as? CustomerInfoCell else {
            fatalError("The dequeued cell is not an instance of CustomerInfoCell.")
        }
        cell.selectionStyle = .none
        cell.setupCellView(row: indexPath.row)
        cell.titleLabel.text = titleTexts[indexPath.row]
        cell.descriptionLabel.text = descriptionTexts[indexPath.row]
        
        return cell
    }
    
    
}
