//
//  TopMenuView.swift
//  Choku_ios
//
//  Created by chitra bonik on 23/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

public protocol TopMenuViewDelegate: class {
    func closeMenuView()
}

class TopMenuView: UIView {

    @IBOutlet weak var menuTabelView: UITableView!
    var delegate: TopMenuViewDelegate?
    
    var popupConfirmView: PopupConfirmView = {
        let view: PopupConfirmView = (Bundle.main.loadNibNamed("PopupConfirmView", owner: self, options: nil))?[0] as! PopupConfirmView
        view.frame = CGRect(x:0 , y: 0, width: SIZE_WIDTH, height: SIZE_HEIGHT)
        view.isHidden = false
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setPropertiesOfView()
        registerClassForCell()
    }
    
    func configureSelf() {
        // Configure self
        Bundle.main.loadNibNamed(String(describing: TopMenuView.self), owner: self, options: nil)
        self.addSubview(self)
    }
    
    func show() {
        if let _window = UIApplication.shared.keyWindow {
            _window.addSubview(self)
            self.isHidden = false
        }
    }
    
    // MARK: - Private function
    private func registerClassForCell() {
        // Register Home Meal list cell
        let nibCell = UINib.init(nibName: "TopMenuCell", bundle: nil)
        menuTabelView.register(nibCell, forCellReuseIdentifier: "TopMenuCell")
    }
    
    private func setPropertiesOfView() {
        menuTabelView.reloadData()
    }
    
    
    @IBAction func tapToClose(_ sender: Any) {
        if let _delegate = delegate {
            _delegate.closeMenuView()
        }
    }
    
}

extension TopMenuView: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 37.0 * DISPLAY_SCALE
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: TopMenuCell = tableView.dequeueReusableCell(withIdentifier: "TopMenuCell", for: indexPath) as! TopMenuCell
        cell.pointLabel.isHidden = true
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = UIText.MenuViewTermsOfService.localized
            break
        case 1:
            cell.titleLabel.text = UIText.MenuViewHelp.localized
            break
        case 2:
            cell.titleLabel.text = UIText.MenuViewLogout.localized
            break
        default:
            cell.titleLabel.text = UIText.MenuViewVersionInformation.localized
            cell.pointLabel.text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            cell.pointLabel.isHidden = false
            break
        }
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 2:
            popupConfirmView.screenName = "logout"
            popupConfirmView.show(title: UIText.PopupLogoutTitle.localized)
            popupConfirmView.delegate = self
            break
        default:
            break
        }
    }
}

extension TopMenuView: PopupConfirmViewDelegate {
    func closeAllView() {
        if let _delegate = delegate {
            _delegate.closeMenuView()
        }
        self.isHidden = true
        self.removeFromSuperview()
    }
}
