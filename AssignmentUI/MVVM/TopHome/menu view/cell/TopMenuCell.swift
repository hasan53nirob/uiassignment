//
//  TopMenuCell.swift
//  Choku_ios
//
//  Created by chitra bonik on 23/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class TopMenuCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUITexts()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setUITexts() {
        titleLabel.font = UIFont.NotoSansCJKJPRegularFont(ofSize: 15.scale())
        titleLabel.textColor = UIColor._ashDarkColor()
        
        pointLabel.font = UIFont.NotoSansCJKJPRegularFont(ofSize: 15.scale())
        pointLabel.textColor = UIColor._ashDarkColor()
    }
    
}
