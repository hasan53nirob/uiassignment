//
//  TopHomeViewController.swift
//  Choku_ios
//
//  Created by chitra bonik on 17/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class TopHomeViewController: BaseViewController {
    
    @IBOutlet weak var mCarStoreLabel: UILabel!
    @IBOutlet weak var mUserStoreLabel: UILabel!
    @IBOutlet weak var mNumberOfCustomerLabel: UILabel!
    @IBOutlet weak var mNumberOfCustomer: UILabel!
    @IBOutlet weak var mSearchTextFeild: UITextField!
    @IBOutlet weak var mMemberRegistration: UILabel!
    @IBOutlet weak var mRegistrationLabel: UILabel!
    @IBOutlet weak var mCustomerList: UILabel!
    @IBOutlet weak var mListLabel: UILabel!
    @IBOutlet weak var mDelivery: UILabel!
    @IBOutlet weak var mDeliveryLabel: UILabel!
    @IBOutlet weak var mDeliveredDate: UILabel!
    @IBOutlet weak var mNews: UILabel!
    @IBOutlet weak var mNewsLabel: UILabel!
    var isMenuExpanded: Bool = false
    let overlayView = UIView()
    
    var menuView: TopMenuView = {
        let view: TopMenuView = (Bundle.main.loadNibNamed("TopMenuView", owner: self, options: nil))?[0] as! TopMenuView
        view.frame = CGRect(x:0 , y: 0, width: 254.0 * DISPLAY_SCALE, height: SIZE_HEIGHT)
        view.isHidden = false
        return view
    }()
    
    var popup: MessageShowPopupView = {
        let view: MessageShowPopupView = (Bundle.main.loadNibNamed("MessageShowPopupView", owner: self, options: nil))?[0] as! MessageShowPopupView
        view.frame = CGRect(x:0 , y: 0, width: SIZE_WIDTH, height: SIZE_HEIGHT)
        view.isHidden = false
        return view
    }()
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        callAPI()
        setUITexts()
        propertiesOfView()
        configureGestures()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        Utils.customTabbarVC?.setHideTitleTabar(isHidden: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        overlayView.frame = view.bounds
        self.menuView.transform = CGAffineTransform(translationX: -(254.0 * DISPLAY_SCALE), y: 0)

    }
    
    //MARK: Private Function
    // Set UITexts
    private func setUITexts() {
        
        mCarStoreLabel.text = UIText.TopHomeCarStoreLabel.localized
        mCarStoreLabel.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 17.scale())
        mCarStoreLabel.textColor = .white
        
        mUserStoreLabel.text = UIText.TopHomeUserStoreLabel.localized
        mUserStoreLabel.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 34.scale())
        mUserStoreLabel.textColor = .white
        
        mNumberOfCustomerLabel.text = UIText.TopHomeNumberOfCustomerTitle.localized
        mNumberOfCustomerLabel.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 12.scale())
        mNumberOfCustomer.font = UIFont.FuturaMediumFont(ofSize: 23.scale())
        
        mSearchTextFeild.placeholder = UIText.TopHomeSearchText.localized
        mSearchTextFeild.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 18.scale())
        
        mMemberRegistration.text = UIText.TopHomeRegistrationJapaneseLabel.localized
        mMemberRegistration.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 18.scale())
        mRegistrationLabel.text = UIText.TopHomeRegistrationEnglishLabel.localized
        mRegistrationLabel.font = UIFont.FuturaMediumFont(ofSize: 12.scale())
        
        mCustomerList.text = UIText.TopHomeListJapaneseLabel.localized
        mCustomerList.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 18.scale())
        mListLabel.text = UIText.TopHomeListEnglishLabel.localized
        mListLabel.font = UIFont.FuturaMediumFont(ofSize: 12.scale())
        
        mDelivery.text = UIText.TopHomeDeliveryJapaneseLabel.localized
        mDelivery.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 18.scale())
        mDeliveryLabel.text = UIText.TopHomeDeliveryEnglishLabel.localized
        mDeliveryLabel.font = UIFont.FuturaMediumFont(ofSize: 12.scale())
        mDeliveredDate.text = UIText.TopHomeDeliveredDateLabel.localized
        mDeliveredDate.font = UIFont.NotoSansCJKJPRegularFont(ofSize: 11.scale())
        
        mNews.text = UIText.TopHomeNewsJapaneseLabel.localized
        mNews.font = UIFont.NotoSansCJKJPMediumFont(ofSize: 18.scale())
        mNewsLabel.text = UIText.TopHomeNewsEnglishLabel.localized
        mNewsLabel.font = UIFont.FuturaMediumFont(ofSize: 12.scale())

    }
    
    private func propertiesOfView() {
        overlayView.backgroundColor = .clear
        overlayView.alpha = 0
        menuView.delegate = self
        view.addSubview(overlayView)
        view.addSubview(menuView)
    }
    
    private func configureGestures() {
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(didSwipeLeft))
        swipeLeftGesture.direction = .left
        overlayView.addGestureRecognizer(swipeLeftGesture)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOverlay))
        overlayView.addGestureRecognizer(tapGesture)
    }
    
    private func callAPI() {
        let token = UserDefaults.standard.object(forKey: kUserDefaultAccessToken) as? String
        if token != nil {
            /*self.showHUDProgress()
            let params:[String:Any] = [APIKey.Token: token!]
            APIBase.shareObject.callTotalCustomer(params: params) { (result, error) in
                self.hideHUDProgress()
                if let _result = result {
                    self.mNumberOfCustomer.text = String(_result)
                } else {
                    if error?.errorCode == 401 {
                        Utils.showAlert(ErrorMessage.ExpiredToken.localized, in: self, completion: {
                            self.menuView.isHidden = true
                        })
                    } else {
                        self.popup.show(message: error!.errorMessage!)
                    }
                    
                }
            }*/
        }
        
    }
    
    private func toggleMenu() {
        isMenuExpanded = !isMenuExpanded
        let xPosition: CGFloat = (isMenuExpanded) ? 0.0 : -(self.menuView.frame.width)
        UIView.animate(withDuration: 0.3, animations: {
            self.menuView.transform = CGAffineTransform(translationX: xPosition, y: 0)
            self.overlayView.alpha = (self.isMenuExpanded) ? 0.5 : 0.0
        }) { (success) in
        }
    }
    //MARK: Objective-C func
    
    @objc fileprivate func didSwipeLeft() {
        toggleMenu()
    }
    
    @objc fileprivate func didTapOverlay() {
        toggleMenu()
    }
    
    //MARK: Button Action
    @IBAction func tapToOpenMenu(_ sender: Any) {
        toggleMenu()
    }
    
    
    @IBAction func tapForRegistration(_ sender: Any) {
        let customTabbarController = CustomTabbarController()
        customTabbarController.viewTabbar.tapToCustomerList(nil)
        self.navigationController?.pushViewController(customTabbarController, animated: true)
    }
    
    @IBAction func tapForOpenCustomerList(_ sender: Any) {
        let customTabbarController = CustomTabbarController()
        customTabbarController.viewTabbar.tapToCustomerList(nil)
        self.navigationController?.pushViewController(customTabbarController, animated: true)
    }
    
    @IBAction func tapToDeliveryButton(_ sender: Any) {
        let customTabbarController = CustomTabbarController()
        customTabbarController.viewTabbar.tapToDelivery(nil)
        self.navigationController?.pushViewController(customTabbarController, animated: true)
    }
    
    @IBAction func tapForNews(_ sender: Any) {
        let customTabbarController = CustomTabbarController()
        customTabbarController.viewTabbar.tapToNotice(nil)
        self.navigationController?.pushViewController(customTabbarController, animated: true)
        
    }
    
    
}

extension TopHomeViewController: TopMenuViewDelegate {
    func closeMenuView() {
        toggleMenu()
    }
}
