//
//  NHTheme.swift
//  AssignmentUI
//
//  Created by Nirob Hasan on 18/11/19.
//  Copyright © 2019 Nirob Hasan. All rights reserved.
//

import UIKit

// MARK: - Singleton

final class NHTheme {
    
    // MARK: Shared Instance
    static let shared = NHTheme()
    
    // MARK: Local Variable
    
    var THEME_VAV_BAR_GRADIENT_1_COLOR:UIColor = UIColor.clear
    var THEME_VAV_BAR_GRADIENT_2_COLOR:UIColor = UIColor.clear
    var THEME_TEXT_COLOR:UIColor = UIColor.clear
    var THEME_TITLE_WHITE_COLOR:UIColor = UIColor.clear
    var THEME_BUTTON_RED_COLOR:UIColor = UIColor.clear
    var THEME_BG_COLOR:UIColor = UIColor.clear
    var THEME_TITLE_GRAY_COLOR:UIColor = UIColor.clear
    var THEME_UNDERLINE_GRAY_COLOR:UIColor = UIColor.clear
    var THEME_SWITCH_GRAY_COLOR:UIColor = UIColor.clear
    
    
    private init() {
        //THEME_VAV_BAR_GRADIENT_1 = UIColor(hexString:"#F0F3F4")
        THEME_VAV_BAR_GRADIENT_1_COLOR = UIColor.init(red: 73, green: 131, blue: 212)
        THEME_VAV_BAR_GRADIENT_2_COLOR = UIColor.init(red: 124, green: 223, blue: 200)
        THEME_TEXT_COLOR = UIColor.init(red: 32, green: 32, blue: 32)
        THEME_TITLE_WHITE_COLOR = UIColor.init(red: 255, green: 255, blue: 255)
        THEME_BUTTON_RED_COLOR = UIColor.init(red: 219, green: 55, blue: 56)
        THEME_BG_COLOR = UIColor.init(red: 238, green: 242, blue: 243)
        THEME_TITLE_GRAY_COLOR = UIColor.init(red: 100, green: 100, blue: 100)
        THEME_UNDERLINE_GRAY_COLOR = UIColor.init(red: 211, green: 211, blue: 211)
        THEME_SWITCH_GRAY_COLOR = UIColor.init(red: 179, green: 179, blue: 179)
     
    }
}
