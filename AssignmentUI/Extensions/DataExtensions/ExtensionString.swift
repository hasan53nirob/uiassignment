//
//  ExtensionString.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

extension String {
    // Localized string
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, bundle: bundle, value: "**\(self)**", comment: "")
    }
    
    func isValidEmail() -> Bool {
        // MARK: - Check string format
        var isValid : Bool = true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if emailTest.evaluate(with: self) == false {
            //Mail is wrong format
            isValid = false
        }
        return isValid
    }
    
    func isValidPassword() -> Bool {
        // MARK: - Check string format
        var isValid : Bool = true
        let passwordRegEx = "(?=.*[A-Z])(?=.*[a-z]).{10,}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        if passwordTest.evaluate(with: self) == false {
            //Password is wrong format
            isValid = false
        }
        return isValid
    }
    
    func convertStringToDate(formatter: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current // set locale to reliable US_POSIX
        let date = dateFormatter.date(from:self)!
        return date
    }
    
    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }
        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }

}
