//
//  ExtensionNumber.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

extension Double {
    func scale() -> CGFloat {
        return CGFloat(self) * UIScreen.main.bounds.width/375
    }
}

extension Int {
    func scale() -> CGFloat {
        return CGFloat(self) * UIScreen.main.bounds.width/375
    }
}

extension CGFloat {
    func scale() -> CGFloat {
        return CGFloat(self) * UIScreen.main.bounds.width/375
    }
}
