//
//  ExtensionDate.swift
//  Choku_ios
//
//  Created by chitra bonik on 4/11/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

extension Date {
    func getDateWithFormat(format: String) -> String {
        let dtFormat = DateFormatter()
        dtFormat.dateFormat = format
        return dtFormat.string(from: self)
    }
    
    // Get the String Value of the Year
    func getNameOfYear() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY"
        return formatter.string(from: self)
    }
    
    // Get the String value of the Month
    func getNameOfMonth() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM"
        return formatter.string(from: self)
    }
    
    // Get weekday
    func getNameOfWeek() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter.string(from: self)
    }
    
    // Get the number of the day
    func getDay() -> NSInteger {
        return NSCalendar.current.component(.day, from: self)
    }
    
    // Convert Date to String
    func convertToString(formatter: String) -> String? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatter
        let myString = dateFormatterGet.string(from: self)
        return myString
    }
    
}
