//
//  ExtensionDevice.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

extension UIDevice {
    
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    
    var iPhoneXSMax: Bool {
        return UIScreen.main.nativeBounds.height == 2688
    }
    
    var iPhoneXR: Bool {
        return UIScreen.main.nativeBounds.height == 1792
    }
}
