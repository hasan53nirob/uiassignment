//
//  ExtensionUIFont.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

extension UIFont {
    class func NotoSansCJKJPBoldFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "NotoSansCJKjp-Bold", size: ofSize)!
    }
    
    class func NotoSansCJKJPMediumFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "NotoSansCJKjp-Medium", size: ofSize)!
    }
    
    class func NotoSansCJKJPRegularFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "NotoSansCJKjp-Regular", size: ofSize)!
    }
    
    class func HiraginoSansW6Font(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "HiraginoSans-W6", size: ofSize)!
    }
    
    class func FuturaMediumFont(ofSize: CGFloat) -> UIFont {
        return UIFont(name: "Futura-Medium", size: ofSize)!
    }
    
}
