//
//  UIPrimaryButtonWhite.swift
//  Choku_ios
//
//  Created by chitra bonik on 8/11/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class UIPrimaryButtonWhite: UIButton {

    override func draw(_ rect: CGRect) {
        //Code
        setTitleColor(UIColor._ashLightColorwith(opacity: 0.7), for: .normal)
        setBackgroundImage(UIImage(named: "btn_white"), for: .normal)
        titleLabel?.font = UIFont.HiraginoSansW6Font(ofSize: 16 * DISPLAY_SCALE)
        self.letterSpacing(spacing: 1.45)
        titleLabel?.textAlignment = .center
        if titleEdgeInsets.top == 0 {
            titleEdgeInsets = UIEdgeInsets(top: titleEdgeInsets.top - (0 * DISPLAY_SCALE), left: titleEdgeInsets.left, bottom: titleEdgeInsets.bottom, right: titleEdgeInsets.right)
        }
    }

}
