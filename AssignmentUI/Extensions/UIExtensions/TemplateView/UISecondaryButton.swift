//
//  UISecondaryButton.swift
//  Choku_ios
//
//  Created by chitra bonik on 23/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class UISecondaryButton: UIButton {

    override func draw(_ rect: CGRect) {
        //Code
        setTitleColor(UIColor.white, for: .normal)
        setBackgroundImage(UIImage(named: "button_gray"), for: .normal)
        titleLabel?.font = UIFont.NotoSansCJKJPBoldFont(ofSize: 16 * DISPLAY_SCALE)
        titleLabel?.textAlignment = .center
        if titleEdgeInsets.top == 0 {
            titleEdgeInsets = UIEdgeInsets(top: titleEdgeInsets.top - (2 * DISPLAY_SCALE), left: titleEdgeInsets.left, bottom: titleEdgeInsets.bottom, right: titleEdgeInsets.right)
        }
    }
}
