//
//  UIText.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

enum UIText: String {
    
    //Login Page
    case LoginViewLabelEmail = "loginview_label_email"
    case LoginViewLabelPassword = "loginview_label_password"
    case LoginViewErrorMessage = "loginview_error_message"
    case LoginViewErrorMessageEmail = "loginview_error_message_email"
    case ForgetPasswordSendMail = "forget_password_send_mail"
    case CreateAccountSendMail = "create_account_send_mail"
    case InputCodeViewCanNotOpenMail = "inputcodeview_mail_can_not_open"
    case LoginViewForgetPasswordLink = "loginview_forget_password_link"
    
    // Loading view
    case LoadingViewDataLoadingLabel = "loadingview_data_loading_label"
    case LoadingViewLoadingLabel = "loadingview_loading_label"
    
    // Logout
    case logoutTitleLabel = "logout_title_label"
    
    //TopHome
    case TopHomeCarStoreLabel = "top_home_car_store_label"
    case TopHomeUserStoreLabel = "top_home_user_store_label"
    case TopHomeNumberOfCustomerTitle = "top_home_number_of_customer_title"
    case TopHomeRegistrationJapaneseLabel = "top_home_registration_japanese_label"
    case TopHomeRegistrationEnglishLabel = "top_home_registration_english_label"
    case TopHomeListJapaneseLabel = "top_home_list_japanese_label"
    case TopHomeListEnglishLabel = "top_home_list_english_label"
    case TopHomeDeliveryJapaneseLabel = "top_home_delivery_japanese_label"
    case TopHomeDeliveryEnglishLabel = "top_home_delivery_english_label"
    case TopHomeDeliveredDateLabel = "top_home_delivered_date_label"
    case TopHomeNewsJapaneseLabel = "top_home_news_japanese_label"
    case TopHomeNewsEnglishLabel = "top_home_news_english_label"
    
    //Menu view
    case MenuViewTermsOfService = "menu_view_terms_of_service"
    case MenuViewHelp = "menu_view_help"
    case MenuViewLogout = "menu_view_logout"
    case MenuViewVersionInformation = "menu_view_version_information"
    
    // New Delivery Page
    case NewDelivaryNavigationTitle = "new_delivary_navigation_title"
    case DeliveryReservationTitle = "delivery_reservation_title"
    case CouponTitle = "coupon_title"
    case DeliveryNameTitle = "delivery_name_title"
    case ContentTitle = "content_title"
    case BannerTitle = "banner_title"
    case ConditionTitle = "condition_title"
    case Gender = "gender"
    case Age = "age"
    case VehicleVerificationDaysUntilExpirationDate = "vehicle_verification_days_until_expiration_date"
    case ElapsedDaysFromMemberRegistration = "elapsed_days_from_member_registration"
    case NumberDaysSinceLastDeliveryDate = "number_days_since_last_delivery_date"
    case NumberDaysSinceLastRefueling = "number_of_days_since_the_last_refueling"
    case ElapsedDaysFromTheDateOfTheLastCarWash = "elapsed_days_from_the_date_of_the_last_car_wash"
    case NumberOfDaysElapsedSinceTheLastCarWash = "number_of_days_elapsed_since_the_last_car_wash"
    case ElapsedDaysFromLastCoatingDate = "elapsed_days_from_last_coating_date"
    case LastOilChange = "last_oil_change"
    case LastTireChange = "last_tire_change"
    case LastDateOfVehicleInspection = "last_date_of_vehicle_inspection"
    case DaysSinceLastInspectionDate = "days_since_last_inspection_date"
    case VehicleVerificationVehicleType = "vehicle_verification_vehicle_type"
    case DaysSinceFirstRegistration = "days_since_first_registration"
    case NumberOfDaysSinceIssueDate = "number_of_days_since_issue_date"
    case CarCompartment = "car_compartment"
    case Year = "year"
    case EstimatedMileageInkm = "estimated_mileage_in_km"
    case CarName = "car_name"
    case VehicleType = "vehicle_type"
    case TotalDisplacement = "total_displacement"
    
    //Edit Page
    case OilChange = "oil_change"
    case NumberOfDaysSinceTheFirstVisit = "number_of_days_since_the_first_visit"
    case EditLastDeliveryDate = "edit_last_delivery_date"
    case NumberOfDaysThatHaveElapsedSinceTheLastRefueling = "number_of_days_that_have_elapsed_since_the_last_refueling"
    case LastCarWash = "last_car_wash"
    case NumberOfDaysLastCarWash = "number_of_days_last_car_wash"
    case PassedSinceTheLastCoatingDate = "passed_since_the_last_coating_date"
    case EditLastOilChange = "edit_last_oil_change"
    case EditLastTireChange = "edit_last_tire_change"
    case PassedDaysSinceLastInspectionDate = "passed_days_since_last_inspection_date"
    case DaysPassedSinceLastInspection = "days_passed_since_last_inspection"
    case SeparateForPrivateUseAndBusinessUse = "separate_for_private_use_and_business_use"
    case FuelType = "fuel_type"
    case BoardingCapacity = "boarding_capacity"
    case DaysThatHavePassedSinceTheDateOfIssue = "days_that_have_passed_since_the_date_of_issue"
    
    //Delivery Terget
    case DeliveryTergetNavigationTitle = "delivery_terget_navigation_title"
    
    //Popup view
    case PopupLogoutTitle = "popup_logout_title"
    case PopupEditCustomerWarningTitle = "popup_edit_customer_warning_title" // zeplin(C25&C33)& pt(C13)
    case popupEditCustomerLineDoneTitle = "popup_edit_customer_line_done_title"//D03
    case popupDeliveryConfirmationReservationTitle = "popup_delivery_confirmation_reservation_title"//E07_a
    case popupDeliveryConfirmationTitle = "popup_delivery_confirmation_title"//E07_b
    case popupDeliveryConfirmationDoneTitle = "popup_delivery_confirmation_done_title"//E08
    
    //Footer
    case HomeLabel = "home_label"
    case CustomerListLabel = "customer_list_label"
    case ScanLabel = "scan_label"
    case NoticeLabel = "notice_label"
    case DeliveryLabel = "delivery_label"
    
    // MARK: - PLACEHOLDER TEXTS
    case PHEmail = "ph_email"
    case PHPasswordResetEmail = "ph_password_reset_email"
    case PHPassword = "ph_password"
    case TopHomeSearchText = "top_home_search_text"
    case Unselected = "unselected"
    case EnterTitleHere = "enter_title_here"
    case PutContentsHere = "put_the_contents_here"
    case Vitz = "vitz"
    
    // MARK: - Common
    case CloseText = "close_text"
    case ReturnText = "return_text"

    // Loacalized
    var localized: String {
        var languageBundle : Bundle?
        let languageCode = UserDefaults.standard
        if UserDefaults.standard.value(forKey: kUserDefaultLanguage) != nil {
            let language = languageCode.string(forKey: kUserDefaultLanguage)!
            if let path  = Bundle.main.path(forResource: language, ofType: "lproj") {
                languageBundle =  Bundle(path: path)
            } else {
                languageBundle = Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj")!)
            }
            return self.rawValue.localized(bundle: languageBundle!, tableName: "UIText")
        } else {
            languageCode.set("ja", forKey: kUserDefaultLanguage)
            languageCode.synchronize()
            let language = languageCode.string(forKey: kUserDefaultLanguage)!
            if let path  = Bundle.main.path(forResource: language, ofType: "lproj") {
                languageBundle =  Bundle(path: path)
            } else{
                languageBundle = Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj")!)
            }
            return self.rawValue.localized(bundle: languageBundle!, tableName: "UIText")
        }
    }
}
