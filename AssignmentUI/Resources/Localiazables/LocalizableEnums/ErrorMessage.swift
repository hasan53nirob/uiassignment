//
//  ErrorMessage.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

enum ErrorMessage: String {
    
    //Email
    case EmptyEmail = "error_empty_email"
    
    // Password
    case EmptyPassword = "error_empty_password"
    
    //Expired token
    case ExpiredToken = "expired_token"
    
    var localized: String {
        
        //        return self.rawValue.localized(tableName: "ErrorMessage")
        
        var languageBundle : Bundle?
        let languageCode = UserDefaults.standard
        if UserDefaults.standard.value(forKey: kUserDefaultLanguage) != nil {
            let language = languageCode.string(forKey: kUserDefaultLanguage)!
            if let path  = Bundle.main.path(forResource: language, ofType: "lproj") {
                languageBundle =  Bundle(path: path)
            } else {
                languageBundle = Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj")!)
            }
            return self.rawValue.localized(bundle: languageBundle!, tableName: "ErrorMessage")
        } else {
            languageCode.set("ja", forKey: kUserDefaultLanguage)
            languageCode.synchronize()
            let language = languageCode.string(forKey: kUserDefaultLanguage)!
            if let path  = Bundle.main.path(forResource: language, ofType: "lproj") {
                languageBundle =  Bundle(path: path)
            } else{
                languageBundle = Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj")!)
            }
            return self.rawValue.localized(bundle: languageBundle!, tableName: "ErrorMessage")
        }
    }
}
