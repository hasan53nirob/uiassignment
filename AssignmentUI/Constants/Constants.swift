//
//  Constants.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

let SIZE_WIDTH: CGFloat = UIScreen.main.bounds.size.width
let SIZE_HEIGHT: CGFloat = UIScreen.main.bounds.size.height

let DISPLAY_SCALE: CGFloat = SIZE_WIDTH / 375.0
let DISPLAY_SCALE_IPAD: CGFloat = SIZE_WIDTH / 768.0

//MARK:- Device
let IS_IPAD = UI_USER_INTERFACE_IDIOM() == .pad


//MARK: - UserDefault Name
let kUserIsRunning = "kUserIsRunning"
let kUserDefaultUserId = "kUserDefaultUserID"
let kUserProfileSteps = "kUserProfileSteps"
let kUserType = "kUserType"
let kUserDefaultRefreshToken = "kUserDefaultRefreshToken"
let kUserDefaultPushDeviceToken = "kUserDefaultPushDeviceToken"
let kUserDefaultAccessToken = "kUserDefaultAccessToken"
let kUserDefaultFirebaseToken = "kUserDefaultFirebaseToken"
let kUserDefaultExpireToken = "kUserDefaultExpireToken"
let kUserDefaultUserListLastMessageId = "kUserDefaultUserListLastMessageId"
let kUserDefaultUnreadCountNotice = "kUserDefaultUnreadCountNotice"
let kUserDefaultEmail = "kEmail"
let kUserDefaultPassword = "kPassword"
let kRefereshTokenInProgress = "kRefreshTokenInProgress"
let kShowCheckinHomeNotification = "kShowCheckinHomeNotification"
let kPlanArraivalMessage = "kPlanArraivalMessage"
let kUserDefaultLanguage = "kLanguage"
let kUserDefaultName = "kName"
let kUserDefaultExpired = "kExpired"
