//
//  Enums.swift
//  Choku_ios
//
//  Created by chitra bonik on 29/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class Enums {

    enum storyboardType: Int {
        case list     = 0
        case scan     = 1
        case notice   = 2
        case delivery = 3
    }
}
