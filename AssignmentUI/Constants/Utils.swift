//
//  Utils.swift
//  Choku_ios
//
//  Created by chitra bonik on 10/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

func log(_ msg: Any?) {
    #if DEBUG
    if let _msg = msg { print(_msg) }
    #endif
}

func showAlert(message:String){
    
    let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: "OK", style: .default))
    
    //show alert on main window
    let _window = UIApplication.shared.keyWindow
    _window?.rootViewController?.present(alertController, animated: true, completion: nil)
}


func testUI(_ imageName:String) {
    
    let window = UIApplication.shared.keyWindow
    //add image view
    if let _window  = window {
        let imageView   = UIImageView(frame: _window.frame)
        imageView.image = UIImage(named: imageName)
        imageView.alpha = 0.3
        _window.addSubview(imageView)
    }
}

class Utils {
    
    // MARK: - Constants
    static let mainWindow: UIWindow? = UIApplication.shared.delegate?.window as? UIWindow
    static let screenSize: CGRect    = UIScreen.main.bounds
    static let isWideScreen: Bool    = Utils.screenSize.height >= 568.0
    static let displayScale: CGFloat = Utils.screenSize.width / 375.0
    static var customTabbarVC: CustomTabbarController? = nil
    // Date Formats
    static let yyyy_MM_dd = "yyyy-MM-dd"
    static let yyyy_MM_dd_HH_mm = "yyyy/MM/dd HH:mm"
    
//    var popup: MessageShowPopupView = {
//
//        return view
//    }()
    
    // Fonts
    static let mainFont: UIFont      = UIFont.systemFont(ofSize: 15.0 * Utils.displayScale)
    
    // Get top padding
    static func getTopPadding() -> CGFloat {
        var topPadding: CGFloat = 20.0
        
        if #available(iOS 11.0, *) {
            if let appDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
                topPadding = max(appDelegate.window?.safeAreaInsets.top ?? 0.0, 20.0)
            }
        }
        return topPadding
    }
    
    // Get bottom padding
    static func getBottomPadding() -> CGFloat {
        var bottomPadding: CGFloat = 0.0
        
        if #available(iOS 11.0, *) {
            if let appDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
                bottomPadding = max(appDelegate.window?.safeAreaInsets.bottom ?? 0.0, 0.0)
            }
        }
        return bottomPadding
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func updateFontSizeControl (controls:Array<Any>, color:UIColor, font:UIFont, space:CGFloat) {
        for item in controls {
            if item is UILabel {
                let label = item as? UILabel
                label?.font = font
                label?.textColor = color
                let attributedTitle = NSMutableAttributedString(string: label?.text ?? "")
                let paragraphStyleTitle = NSMutableParagraphStyle()
                paragraphStyleTitle.lineSpacing = space
                attributedTitle.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyleTitle, range:NSMakeRange(0, attributedTitle.length))
                label?.attributedText = attributedTitle
            }
            if item is UIButton {
                let button = item as? UIButton
                button?.titleLabel?.font = font
                button?.setTitleColor(color, for: .normal)
                button?.setTitleColor(color, for: .selected)
                let attributedTitle = NSMutableAttributedString(string: button?.titleLabel?.text ?? "")
                let paragraphStyleTitle = NSMutableParagraphStyle()
                paragraphStyleTitle.lineSpacing = space
                attributedTitle.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyleTitle, range:NSMakeRange(0, attributedTitle.length))
                button?.setAttributedTitle(attributedTitle, for: .normal)
                button?.setAttributedTitle(attributedTitle, for: .selected)
            }
            if item is UITextField {
                let textField = item as? UITextField
                textField?.font = font
                textField?.textColor = color
                let attributedTitle = NSMutableAttributedString(string: textField?.text ?? "")
                let paragraphStyleTitle = NSMutableParagraphStyle()
                paragraphStyleTitle.lineSpacing = space
                attributedTitle.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyleTitle, range:NSMakeRange(0, attributedTitle.length))
                textField?.attributedText = attributedTitle
            }
            if item is UITextView {
                let textView = item as? UITextView
                textView?.font = font
                textView?.textColor = color
                let attributedTitle = NSMutableAttributedString(string: textView?.text ?? "")
                let paragraphStyleTitle = NSMutableParagraphStyle()
                paragraphStyleTitle.lineSpacing = space
                attributedTitle.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyleTitle, range:NSMakeRange(0, attributedTitle.length))
                textView?.attributedText = attributedTitle
            }
            
        }
    }
    
    static func showAlert(_ alertMessage: String, in viewController: UIViewController? = nil, completion: (() -> Void)? = nil) {
        if let appDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
            // Init alert
            let alertController: UIAlertController = UIAlertController(title: nil, message: alertMessage, preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
                //Cancel Action
            }))
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert) in
                completion?()
                Utils.removeAllSaveData()
            }))
            
            // Show alert
            if viewController != nil {
                viewController?.present(alertController, animated: true, completion: nil)
            } else if let _visibleViewController = self.getVisibleViewController(nil) {
                _visibleViewController.present(alertController, animated: true, completion: nil)
            } else {
                if let _window = appDelegate.window, let _rootViewController = _window.rootViewController {
                    _rootViewController.present(alertController, animated: true, completion: nil)
                }
            }
        }
//        let successPopupMessage: PopupShowMessage = (Bundle.main.loadNibNamed("PopupShowMessage", owner: nil)?[0] as? PopupShowMessage)!
//        successPopupMessage.frame = CGRect(x:0 , y: 0, width: SIZE_WIDTH, height: SIZE_HEIGHT)
//        successPopupMessage.isHidden = false
//        successPopupMessage.show(message: alertMessage)
    }
    
    static func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
        
        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = UIApplication.shared.keyWindow?.rootViewController
        }
        
        if rootVC?.presentedViewController == nil {
            return rootVC
        }
        
        if let presented = rootVC?.presentedViewController {
            if presented.isKind(of: UINavigationController.self) {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }
            
            if presented.isKind(of: UITabBarController.self) {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }
            
            return getVisibleViewController(presented)
        }
        return nil
    }
    
    static func removeAllSaveData(completion: (() -> Void)? = nil) {
        /*APIBase.shareObject.callLogoutAPI(params: nil) { (success, error) in
            if success == true {
                completion?()
                // Remove saved email, password // Model
                UserDefaults.standard.removeObject(forKey: kUserDefaultAccessToken)
                UserDefaults.standard.removeObject(forKey: kUserDefaultUserId)
                UserDefaults.standard.removeObject(forKey: kUserDefaultEmail)
                UserDefaults.standard.removeObject(forKey: kUserDefaultName)
                UserDefaults.standard.synchronize()
                // Pop to LoginVC
                if let appDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate {
                    if let navigationVC: UINavigationController = appDelegate.window?.rootViewController as? UINavigationController {
                        for vc in navigationVC.viewControllers {
                            if (vc is LoginViewController) == true {
                                navigationVC.popToViewController(vc, animated: true)
                            }
                        }
                    }
                }
            } else {
                if error != nil {
                    let popupView: MessageShowPopupView = (Bundle.main.loadNibNamed("MessageShowPopupView", owner: nil, options: nil))?[0] as! MessageShowPopupView
                    popupView.frame = CGRect(x:0 , y: 0, width: SIZE_WIDTH, height: SIZE_HEIGHT)
                    popupView.isHidden = false
                    popupView.show(message: error!.errorMessage!)
                }
            }
        }
        */
    }
    
    static func customPrint(printObject: Any?) {
        #if DEBUG
        if let _printObject = printObject {
            print(_printObject)
        }
        #endif
    }
    

}
