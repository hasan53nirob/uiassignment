//
//  ViewController.swift
//  AssignmentUI
//
//  Created by Nirob Hasan on 18/11/19.
//  Copyright © 2019 Nirob Hasan. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var editCustomerTableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: EditCustomerNameCell.nibName(), bundle: nil)
        editCustomerTableView.register(nib, forCellReuseIdentifier: EditCustomerNameCell.reuseIdentifier())
        
        let nib2 = UINib(nibName: EditCustomerNameCell2.nibName(), bundle: nil)
        editCustomerTableView.register(nib2, forCellReuseIdentifier: EditCustomerNameCell2.reuseIdentifier())
        
        let nib3 = UINib(nibName: EditCustomerMiddleCell.nibName(), bundle: nil)
        editCustomerTableView.register(nib3, forCellReuseIdentifier: EditCustomerMiddleCell.reuseIdentifier())
        
        let nib4 = UINib(nibName: EditCustomerLineCell.nibName(), bundle: nil)
        editCustomerTableView.register(nib4, forCellReuseIdentifier: EditCustomerLineCell.reuseIdentifier())
        
        let nib5 = UINib(nibName: EditCustomerBotomCell.nibName(), bundle: nil)
        editCustomerTableView.register(nib5, forCellReuseIdentifier: EditCustomerBotomCell.reuseIdentifier())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.view.backgroundColor = NHTheme.shared.THEME_BG_COLOR
        
        topView.addGradient(color1: NHTheme.shared.THEME_VAV_BAR_GRADIENT_1_COLOR, color2: NHTheme.shared.THEME_VAV_BAR_GRADIENT_2_COLOR)
        titleLabel.textColor = NHTheme.shared.THEME_TITLE_WHITE_COLOR
        topView.bringSubviewToFront(titleLabel)
        topView.bringSubviewToFront(nextButton)
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //testUI("C12_List(HOME)_EditCustomer")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func testUI(_ imageName:String) {
        
        let window = UIApplication.shared.keyWindow
        //add image view
        if let _window  = window {
            let imageView   = UIImageView(frame: _window.frame)
            imageView.image = UIImage(named: imageName)
            imageView.alpha = 0.3
            _window.addSubview(imageView)
        }
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        print("nextButtonClicked")
        let storyboard = UIStoryboard(name: "CustomerInfo", bundle: nil)
        let customerInfoController = storyboard.instantiateViewController(withIdentifier: CustomerInfoViewController.getIdentifier())
        self.navigationController?.pushViewController(customerInfoController, animated: true)
    }
    
}

extension ViewController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditCustomerNameCell.reuseIdentifier()) as? EditCustomerNameCell else {
                fatalError("The dequeued cell is not an instance of EditCustomerNameCell.")
            }
            cell.selectionStyle = .none
            cell.setupCellView(row: indexPath.row)
            
            return cell
        }
        else if indexPath.row == 1 || indexPath.row == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditCustomerNameCell2.reuseIdentifier()) as? EditCustomerNameCell2 else {
                fatalError("The dequeued cell is not an instance of EditCustomerNameCell2.")
            }
            cell.selectionStyle = .none
            cell.setupCellView(row: indexPath.row)
            
            return cell
        }
        else if indexPath.row == 3 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditCustomerMiddleCell.reuseIdentifier()) as? EditCustomerMiddleCell else {
                fatalError("The dequeued cell is not an instance of EditCustomerMiddleCell.")
            }
            cell.selectionStyle = .none
            cell.setupCellView(row: indexPath.row)
            
            return cell
        }
        else if indexPath.row == 4 || indexPath.row == 5 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: EditCustomerLineCell.reuseIdentifier()) as? EditCustomerLineCell else {
                fatalError("The dequeued cell is not an instance of EditCustomerLineCell.")
            }
            cell.selectionStyle = .none
            cell.setupCellView(row: indexPath.row)
            
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EditCustomerBotomCell.reuseIdentifier()) as? EditCustomerBotomCell else {
            fatalError("The dequeued cell is not an instance of EditCustomerBotomCell.")
        }
        cell.selectionStyle = .none
        cell.setupCellView(row: indexPath.row)
        
        return cell
    }
}

