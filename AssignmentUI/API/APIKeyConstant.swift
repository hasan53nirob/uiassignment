//
//  APIKeyConstant.swift
//  Choku_ios
//
//  Created by chitra bonik on 18/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

let KPROJECT_ID = "1"

struct APIKey {
    //A
    static let Alias = "alias"
    static let Accept = "Accept"
    static let ApplicationJson = "application/json"
    static let Address = "address"
    static let AquisitionCustomers = "aquisition_customers"
    
    //B
    static let Body = "body"
    
    //C
    static let ContentType = "Content-Type"
    static let CreatedAt = "created_at"
    static let CorporateId = "corporate_id"
    static let Code = "code"
    static let CouponId = "coupon_id"
    static let Count = "count"
    
    //D
    static let Data = "data"
    static let DeliveryPreviousHolidayFlag = "delivery_previous_holiday_flag"
    static let DeliveryDate = "delivery_date"
    static let DeliveryTime = "delivery_time"
    static let DeliveryCount = "delivery_count"
    
    //E
    static let Email = "email"
    static let EmailPassword = "email_password"
    static let Error = "error"
    static let Error_Code = "error_code"
    static let ErrorCode = "code"
    static let Expired = "expired"
    
    //F
    static let FirstName = "first_name"
    static let FirstNameKana = "first_name_kana"
    static let FailedLogin = "failed_login"
    static let FailedLoginCounter = "failed_login_counter"
    static let FilterID = "filter_id"
    
    //G
    static let GroupId01 = "group_id_1"
    static let GroupId02 = "group_id_2"
    static let GroupId03 = "group_id_3"
    static let GroupId04 = "group_id_4"
    
    //H
    static let HolidayDayOfWeek = "holiday_day_of_week"
    static let HolidayDayNationalHoliday = "holiday_national_holiday"
    static let HolidayDayDate = "holiday_date"
    static let HasNext = "hasNext"
    static let HasPrev = "hasPrev"
    
    //I
    static let Id = "id"
    static let Image = "image"
    
    //K
    static let Kana = "kana"
    
    //L
    static let LastName = "last_name"
    static let LastNameKana = "last_name_kana"
    static let LastLogin = "last_login"
    static let Language = "language"
    static let Limit = "limit"
    
    //M
    static let Msg = "msg"
    static let Message = "message"
    static let Memo = "memo"
    static let MonthlyAppDeliveryCount = "monthly_app_delivery_count"
    static let MonthlyScenarioDeliveryCount = "monthly_scenario_delivery_count"
    static let MonthlyAdminDeliveryCount = "monthly_admin_delivery_count"
    static let MonthlyMessageCount = "monthly_message_count"
    static let MonthlyReactionCount = "monthly_reaction_count"
    
    //N
    static let Name = "name"
    
    //O
    static let OpenHours = "open_hours"
    static let Option = "option"
    
    //P
    static let Password = "password"
    static let ProfilePic = "profile_pic"
    static let Preference = "preference"
    static let PostCode = "postcode"
    static let PhoneNumber = "phone_number"
    static let Page = "page"
    
    //R
    static let RoleId = "roleId"
    static let Result = "result"
    static let refreshToken = "refresh_token"
    
    //S
    static let Status = "status"
    static let Success = "success"
    static let SuccessResponse = "Success response"
    
    //T
    static let Token = "token"
    static let ThumbSmall = "thumb_small"
    static let ThumbMedium = "thumb_medium"
    static let ThumbLarge = "thumb_large"
    static let Title = "title"
    static let TypeData = "type"
    static let TotalPage = "totalPage"
    
    //U
    static let User = "user"
    static let UserNumber = "user_number"
    static let UserName = "username"
    static let UpdatedAt = "updated_at"
    static let Url = "url"
}
