//
//  APIBase.swift
//  Choku_ios
//
//  Created by chitra bonik on 18/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class APIBase {
    
    static let shareObject = APIBase()
    var header: HTTPHeaders!
    var manager: SessionManager!
    var refreshTokenRequestCounter = 0
    
    // Initialization
    init() {
        header = [String : String]()
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        ;
        manager = Alamofire.SessionManager(configuration: configuration)
    }
    
    func callAPIRequestWithJSONData(requestUrl: String,
                        method: HTTPMethod,
                        params: Parameters?,
                        header: HTTPHeaders?,
                        completionHandler: @escaping(NSDictionary?, NSError?) -> ()) {
        
        // just for print
        Utils.customPrint(printObject: "[REQUEST URL]: \(requestUrl)")
        if let _header = header {
            Utils.customPrint(printObject: "[REQUEST HEADER]: \(_header)")
        }
        if let _param = params {
            Utils.customPrint(printObject: "[REQUEST PARAM]: \(_param)")
        }
        
        Alamofire.request(requestUrl, method: method, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            //            switch(response.result){
            //                case .success(let v)
            //            }
            if let result = response.result.value as? NSDictionary {
                print(result)
                completionHandler(result, nil)
            } else {
                completionHandler(nil, response.result.error as NSError?)
            }
        }
        
        //        Alamofire.request(requestUrl, method: method, parameters: params, headers: header).responseJSON { (response:DataResponse) in
        //            switch(response.result)
        //            {
        //            case .success:
        //                completionHandler(nil, nil)
        //            case .failure(_):
        //                completionHandler(nil, nil)
        //            }
        //
        //        }
        
    }
    
    func callAPIRequestWithEncoding(requestURL: String,
                    method: HTTPMethod,
                    params: Parameters?,
                    header: HTTPHeaders?,
                    completionHandler: @escaping(NSDictionary?, NSError?) -> ()) {
        
        Alamofire.request(requestURL, method: method, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let result = response.result.value as? NSDictionary {
                print(result)
                completionHandler(result, nil)
            } else {
                completionHandler(nil, response.result.error as NSError?)
            }
        }
    }
    
    /*
 
    */
    
    func callAPIRequestWithoutAlamofire(_ url: String, parameters: [String: String], completion: @escaping ([String: Any]?, Error?) -> Void) {
        var components = URLComponents(string: url)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        let request = URLRequest(url: components.url!)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,                            // is there data
                let response = response as? HTTPURLResponse,  // is there HTTP response
                (200 ..< 300) ~= response.statusCode,         // is statusCode 2XX
                error == nil else {                           // was there no error, otherwise ...
                    completion(nil, error)
                    return
            }
            
            let responseObject = (try? JSONSerialization.jsonObject(with: data)) as? [String: Any]
            completion(responseObject, nil)
        }
        task.resume()
    }
    
    // Header with Access Token
    // Used after login
    func getHeaderWithAccessToken() -> [String:String] {
        let accessToken: String = (UserDefaults.standard.object(forKey: kUserDefaultAccessToken) as? String) ?? ""
        return ["Authorization": "bearer \(accessToken)"]
    }
    
    // Return Base64String
    func getBase64String(str: String) -> String {
        let dictionary = "\(str):\(KPROJECT_ID)"
        let base64String = dictionary.toBase64()!
        return base64String
    }
    
}
