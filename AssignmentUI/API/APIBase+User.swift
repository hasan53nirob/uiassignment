//
//  APIBase+User.swift
//  Choku_ios
//
//  Created by chitra bonik on 18/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit
import Alamofire

extension APIBase {
    
    // Login API
    func callLoginAPI(params: Parameters?, completionHandler: @escaping (LoginResponse?, UserProfileResponse?, ErrorResponse?) -> ()) {
        
        let requestUrl = "\(APP_BASE_URL)\(API_POST_LOGIN)"
        var parameter = Parameters()
        if params != nil {
            for key in (params?.keys)! {
                parameter[key] = params?[key]
            }
        }
        
        self.callAPIRequestWithJSONData(requestUrl: requestUrl, method: .post, params: parameter, header: nil) { (result, error) in
            if let _result = result {
                if let _expired = _result[APIKey.Expired] as? Int {
                    UserDefaults.standard.set(_expired, forKey: kUserDefaultExpired)
                }
                if let _data = _result[APIKey.Data] as? NSDictionary {
                    let loginResponse = LoginResponse.init(response: _data)
                    if let _userData = _result[APIKey.User] as? NSDictionary {
                        let userResponse = UserProfileResponse.init(response: _userData)
                        completionHandler(loginResponse,userResponse,nil)
                    } else {
                        completionHandler(loginResponse,nil,nil)
                    }
                    
                } else {
                    let error = self.errorMessageResponse(_result: _result)
                    completionHandler(nil,nil, error)
                }
            }
        }
    }
    
    // Remind Password API
    func callRemindPasswordAPI(params: Parameters?, completionHandler: @escaping (Bool?, ErrorResponse?) -> ()) {
        
        let requestUrl = "\(APP_BASE_URL)\(API_Forget_Password)"
        var parameter = Parameters()
        if params != nil {
            for key in (params?.keys)! {
                parameter[key] = params?[key]
            }
        }
        
        self.callAPIRequestWithJSONData(requestUrl: requestUrl, method: .post, params: parameter, header: nil) { (result, error) in
            if let _result = result {
                if let _success = _result[APIKey.Success] as? Bool {
                    if _success == true {
                        completionHandler(true, nil)
                    } else {
                        let error = self.errorMessageResponse(_result: _result)
                        completionHandler(false, error)
                    }
                } else {
                    let error = self.errorMessageResponse(_result: _result)
                    completionHandler(false, error)
                }
            }
        }
    }
    
    // Delivery Page API
    
    func callGetDeliveryAPI(params: Parameters?, completionHandler: @escaping ([ReservationItemResponse]?,[ReservationItemResponse]?, ErrorResponse?) -> ()) {
        let requestUrl = "\(APP_BASE_URL)\(API_Delivery_Get)"
        var parameter = Parameters()
        if params != nil {
            for key in (params?.keys)! {
                parameter[key] = params?[key]
            }
        }
        
        callAPIRequestWithEncoding(requestURL: requestUrl, method: .get, params: params, header: nil) { (result, error) in
            if let _result = result {
                if let _items = _result[APIKey.Data] as? NSArray {
                    var deliveryList = [ReservationItemResponse]()
                    var deliveryListPre = [ReservationItemResponse]()
                    if _items.count > 0 {
                        deliveryList.removeAll()
                        deliveryListPre.removeAll()
                        for item in _items {
                            if let _data = item as? NSDictionary {
                                if let currentToNext = _data[APIKey.DeliveryDate] as? Int {
                                    let responseDate = Date(timeIntervalSince1970: TimeInterval(currentToNext))
                                    let dateStr = responseDate.convertToString(formatter: Utils.yyyy_MM_dd_HH_mm)
                                    let responseDateDay = responseDate.getDay()
                                    let currentDate = Date()
                                    let currentDay = currentDate.getDay()
                                    if responseDate < currentDate {
                                        if responseDateDay == currentDay {
                                            
                                            let data = ReservationItemResponse(response: _data)
                                            data.deliveryDateString = dateStr
                                            deliveryList.append(data)
                                        } else {
                                            let data = ReservationItemResponse(response: _data)
                                            data.deliveryDateString = dateStr
                                            deliveryListPre.append(data)
                                        }
                                    } else {
                                        let data = ReservationItemResponse(response: _data)
                                        data.deliveryDateString = dateStr
                                        deliveryList.append(data)
                                    }
                                    
                                }
                                
                            }
                        }
                        completionHandler(deliveryList,deliveryListPre,nil)
//                        deliveryList = deliveryList.reversed()
                    } else {
                        completionHandler(nil,nil,nil)
                    }
                } else {
                    let error = self.errorMessageResponse(_result: _result)
                    completionHandler(nil,nil, error)
                }
            }
        }
    }
    
    func errorMessageResponse (_result: NSDictionary) -> ErrorResponse? {
        if let _data = _result[APIKey.Error] as? NSDictionary {
            if _data.count > 0 {
                let errorResponse = ErrorResponse.init(response: _data)
                return errorResponse
            } else {
                if let _message = _result[APIKey.Message] as? String {
                    let error = ErrorResponse()
                    error.errorMessage = _message
                    error.errorCode = _result[APIKey.Code] as? Int ?? -1
                    return error
                }
            }
        } else if let _message = _result[APIKey.Message] as? String {
            let error = ErrorResponse()
            error.errorMessage = _message
            error.errorCode = _result[APIKey.Code] as? Int ?? -1
            return error
        }
        return nil
    }
    
    func callCouponListAPI(params: Parameters?, completionHandler: @escaping ( String?) -> ()) {
        let requestUrl = "\(APP_BASE_URL)\(API_Coupon_List)"
        var parameter = Parameters()
        if params != nil {
            for key in (params?.keys)! {
                parameter[key] = params?[key]
            }
        }
        callAPIRequestWithEncoding(requestURL: requestUrl, method: .get, params: params, header: nil) { (result, error) in
            if let _result = result {
            }
        }
//        self.callAPIRequestWithJSONData(requestUrl: requestUrl, method: .get, params: parameter, header: nil) { (result, error) in
//            if let _result = result {
//                
//            }
//        }
    }
    
    func callTotalCustomer(params: Parameters?, completionHandler: @escaping (Int?, ErrorResponse?) -> ()) {
        let requestUrl = "\(APP_BASE_URL)\(API_Customer_Count)"
        var parameter = Parameters()
        if params != nil {
            for key in (params?.keys)! {
                parameter[key] = params?[key]
            }
        }
        
        callAPIRequestWithEncoding(requestURL: requestUrl, method: .get, params: params, header: nil) { (result, error) in
            if let _result = result {
                if let _success = _result[APIKey.Success] as? Bool {
                    if _success == true {
                        let customerResponse = _result[APIKey.AquisitionCustomers] as? Int
                        completionHandler(customerResponse, nil)
                    } else {
                        let error = self.errorMessageResponse(_result: _result)
                        completionHandler(nil, error)
                    }
                }
                
            }
        }
    }
    
    func callAPITergetList(params: Parameters?, completionHandler: @escaping (Int?, ErrorResponse?) -> ()) {
        let requestUrl = "\(APP_BASE_URL)\(API_Customers_Search)"
        var parameter = Parameters()
        if params != nil {
            for key in (params?.keys)! {
                parameter[key] = params?[key]
            }
        }
        callAPIRequestWithEncoding(requestURL: requestUrl, method: .get, params: params, header: nil) { (result, error) in
            if let _result = result {
                if let _success = _result[APIKey.Success] as? Bool {
                    if _success == true {
                        let customerResponse = _result[APIKey.AquisitionCustomers] as? Int
                        completionHandler(customerResponse, nil)
                    } else {
                        let error = self.errorMessageResponse(_result: _result)
                        completionHandler(nil, error)
                    }
                }
                
            }
        }
    }
    
    func callLogoutAPI(params: Parameters?, completionHandler: @escaping (Bool, ErrorResponse?) -> ()) {
        let requestUrl = "\(APP_BASE_URL)\(API_Logout)"
        var parameter = Parameters()
        if params != nil {
            for key in (params?.keys)! {
                parameter[key] = params?[key]
            }
        }
        
        self.callAPIRequestWithJSONData(requestUrl: requestUrl, method: .get, params: nil, header: getHeaderWithAccessToken()) { (result, error) in
            if let _result = result {
                if let _success = _result[APIKey.Success] as? Bool {
                    if _success == true {
                        completionHandler(true, nil)
                    } else {
                        let error = self.errorMessageResponse(_result: _result)
                        completionHandler(false, error)
                    }
                } else {
                    let error = self.errorMessageResponse(_result: _result)
                    completionHandler(false, error)
                }
            }
        }
    }
    
}
