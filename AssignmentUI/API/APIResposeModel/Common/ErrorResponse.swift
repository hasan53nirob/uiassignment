//
//  ErrorResponse.swift
//  Choku_ios
//
//  Created by chitra bonik on 18/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class ErrorResponse: NSObject {
    var errorCode: Int = -1
    var errorMessage: String?
    
    override init() {
        
    }
    
    init(response: NSDictionary) {
        self.errorCode = response[APIKey.Error_Code] as! Int
        self.errorMessage = response[APIKey.Msg] as? String
    }

}
