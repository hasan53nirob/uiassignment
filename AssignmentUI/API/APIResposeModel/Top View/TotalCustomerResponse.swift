//
//  TotalCustomerResponse.swift
//  Choku_ios
//
//  Created by chitra bonik on 12/11/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class TotalCustomerResponse {

    var aquisitionCustomers: Int?
    var message: String?
    var success: Bool?
    
    init(response: NSDictionary) {
        self.aquisitionCustomers = response[APIKey.AquisitionCustomers] as? Int
        self.message = response[APIKey.Message] as? String
        self.success = response[APIKey.Success] as? Bool
    }
}
