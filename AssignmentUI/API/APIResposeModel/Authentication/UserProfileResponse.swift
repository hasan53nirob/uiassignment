//
//  UserProfileResponse.swift
//  Choku_ios
//
//  Created by chitra bonik on 18/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class UserProfileResponse {
    
    var id: Int?
    var corporateId: String?
    var name: String?
    var kana: String?
    var code: String?
    var groupId01: String?
    var groupId02: String?
    var groupId03: String?
    var groupId04: String?
    var postcode: String?
    var address: String?
    var phoneNumber: String?
    var email: String?
    var openHours: String?
    var holidayDayOfWeek: String?
    var holidayNationalHoliday: Bool?
    var holidayDate: String?
    var deliveryPreviousHolidayFlag: Bool?
    var url: String?
    var option: String?
    var memo: String?
    var message: String?
    var monthlyAppDeliveryCount: String?
    var monthlyScenarioDeliveryCount: String?
    var monthlyAdminDeliveryCount: String?
    var monthlyMessageCount: String?
    var monthlyReactionCount: String?
    
    init(response: NSDictionary) {
        self.id = response[APIKey.Id] as? Int
        self.corporateId = response[APIKey.CorporateId] as? String
        self.name = response[APIKey.Name] as? String
        self.kana = response[APIKey.Kana] as? String
        self.code = response[APIKey.Code] as? String
        self.groupId01 = response[APIKey.GroupId01] as? String
        self.groupId02 = response[APIKey.GroupId02] as? String
        self.groupId03 = response[APIKey.GroupId03] as? String
        self.groupId04 = response[APIKey.GroupId04] as? String
        self.postcode = response[APIKey.PostCode] as? String
        self.address = response[APIKey.Address] as? String
        self.phoneNumber = response[APIKey.PhoneNumber] as? String
        self.email = response[APIKey.Email] as? String
        self.openHours = response[APIKey.OpenHours] as? String
        self.holidayDayOfWeek = response[APIKey.HolidayDayOfWeek] as? String
        self.holidayNationalHoliday = response[APIKey.HolidayDayNationalHoliday] as? Bool
        self.holidayDate = response[APIKey.HolidayDayDate] as? String
        self.deliveryPreviousHolidayFlag = response[APIKey.DeliveryPreviousHolidayFlag] as? Bool
        self.url = response[APIKey.Url] as? String
        self.option = response[APIKey.Option] as? String
        self.memo = response[APIKey.Memo] as? String
        self.message = response[APIKey.Message] as? String
        self.monthlyAppDeliveryCount = response[APIKey.MonthlyAppDeliveryCount] as? String
        self.monthlyScenarioDeliveryCount = response[APIKey.MonthlyScenarioDeliveryCount] as? String
        self.monthlyAdminDeliveryCount = response[APIKey.MonthlyAdminDeliveryCount] as? String
        self.monthlyMessageCount = response[APIKey.MonthlyMessageCount] as? String
        self.monthlyReactionCount = response[APIKey.MonthlyReactionCount] as? String
    }

}
