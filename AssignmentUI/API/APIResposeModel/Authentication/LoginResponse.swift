//
//  LoginResponse.swift
//  Choku_ios
//
//  Created by chitra bonik on 18/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//


import UIKit

struct LoginResponse: Decodable {
    
    var token: String?
    var refreshToken: String?
    
    init(response: NSDictionary) {
        self.token = response[APIKey.Token] as? String
        self.refreshToken = response[APIKey.refreshToken] as? String
    }
    
}
