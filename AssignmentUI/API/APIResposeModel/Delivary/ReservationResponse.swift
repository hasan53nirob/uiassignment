//
//  ReservationResponse.swift
//  Choku_ios
//
//  Created by chitra bonik on 1/11/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class ReservationResponse {

    var token: String?
    
    init(response: NSDictionary) {
        self.token = response[APIKey.Token] as? String
    }
}
