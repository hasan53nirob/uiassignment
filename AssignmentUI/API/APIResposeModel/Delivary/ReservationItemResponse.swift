//
//  ReservationItemResponse.swift
//  Choku_ios
//
//  Created by chitra bonik on 1/11/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit

class ReservationItemResponse {

    var id: Int?
    var corporateId: Int?
    var couponId: Int?
    var filterId: Int?
    var title: String?
    var deliveryDate: Int?
    var deliveryDateString: String?
    var deliveryTime: Int?
    var status: String?
    var body: String?
    var image: String?
    var type: String?
    var deliveryCount: Int?
    var createdAt: String?
    var updatedAt: String?
    
    init() {
        
    }
    
    init(response: NSDictionary) {
        self.id = response[APIKey.Id] as? Int
        self.corporateId = response[APIKey.CorporateId] as? Int
        self.couponId = response[APIKey.CouponId] as? Int
        self.filterId = response[APIKey.FilterID] as? Int
        self.title = response[APIKey.Title] as? String
        self.deliveryDate = response[APIKey.DeliveryDate] as? Int
        //self.deliveryDate = response[APIKey.DeliveryDate] as? Int
        self.deliveryTime = response[APIKey.DeliveryTime] as? Int
        self.status = response[APIKey.Status] as? String
        self.body = response[APIKey.Body] as? String
        self.image = response[APIKey.Image] as? String
        self.type = response[APIKey.TypeData] as? String
        self.deliveryCount = response[APIKey.DeliveryCount] as? Int
        self.createdAt = response[APIKey.CreatedAt] as? String
        self.updatedAt = response[APIKey.UpdatedAt] as? String
    }
}
