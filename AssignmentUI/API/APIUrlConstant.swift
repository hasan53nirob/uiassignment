//
//  APIUrlConstant.swift
//  Choku_ios
//
//  Created by chitra bonik on 18/10/19.
//  Copyright © 2019 Oceanize. All rights reserved.
//

import UIKit


// BASE URL

#if STAGING

let APP_BASE_URL = "https://bd35.ocdev.me/api/"

#elseif DEV

let APP_BASE_URL = "https://bd35.ocdev.me/api/"

#else //PRODUCTION

let APP_BASE_URL = "https://bd35.ocdev.me/api/"

#endif

// LOGIN
let API_POST_LOGIN = "app/users/login"

// Password
let API_Forget_Password = "app/users/password/remind"

//Delivery
let API_Deliveries_Reservation = "app/deliveries/reservation"
let API_Delivery_Get = "app/delivery/get"
let API_Coupon_List = "app/coupons"
let API_Customer_Count = "app/customers/count"
let API_Customers_Search = "app/customers/search"
let API_Logout = "/app/users/logout"
