//
//  BaseRoundedView.swift
//  Khushu
//
//  Created by mac pro-t1 on 3/18/19.
//  Copyright © 2019 Elo. All rights reserved.
//

import UIKit

class RoundedView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupDefaultLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //setupDefaultLayouts()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupDefaultLayouts()
    }
    
    func setupDefaultLayouts() {
        
        self.layer.cornerRadius = 5.0
        //To apply border
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        
        //To apply Shadow
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 1.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0) // Use any CGSize
        self.layer.shadowColor = UIColor.black.cgColor
        
    }
    
}
