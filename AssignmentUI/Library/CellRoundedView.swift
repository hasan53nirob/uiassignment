//
//  BaseRoundedView.swift
//  Khushu
//
//  Created by mac pro-t1 on 3/18/19.
//  Copyright © 2019 Elo. All rights reserved.
//

import UIKit

class CellRoundedView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupDefaultLayouts()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //setupDefaultLayouts()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupDefaultLayouts()
    }
    
    func setupDefaultLayouts() {
        
        self.backgroundColor = NHTheme.shared.THEME_TITLE_WHITE_COLOR
        self.layer.cornerRadius = 3.0
        //To apply border
        self.layer.borderWidth = 0.5
        self.layer.borderColor = NHTheme.shared.THEME_UNDERLINE_GRAY_COLOR.cgColor
        
    }
    
}
